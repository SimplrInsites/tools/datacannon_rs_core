# Data Cannon Core
[![pipeline status](https://gitlab.com/asevans48/datacannon-rs-core/badges/master/pipeline.svg)](https://gitlab.com/asevans48/datacannon-rs-core/-/commits/master)

Data Cannon contains the shared libraries for the client and worker in this distributed
job queue.

This framework is like Celery but for Rust and uses asyncio. Celery v2.0
message protcol is used.

More will follow...

## See


For more information related to job queues:

https://docs.celeryproject.org/en/latest/index.html

## Features

Existing features included in release 0.1:

    - SSL support
    - AMQP/RabbitMQ broker support
    - Elastic Search backend Support
    - Redis backend support
    - RPC backend support
    - Client and Workers
    - Routing Key Support
    - Registry support
    - Message protocol support and serialization
    - Identification matching Celery
    - Threadable connections in worker ;)
    - Tokio support in the clinet ;)
    
Features to include later (0.2+):

    - Creation of a messaging framework like Kombu
    - All other backends
    - OAuth2.0 support (RabbitMQ, Elasticsearch)
    - monitoring support (PRIORITY)
    - healthcecking support (PRIORITY)
    - Rust implemented LevelDB Broker
    - Upgrade Functions
    - Kafka Broker Support
    - Support for an actor system based broker or 0mq style broker
    - Support for additional backends (kafka?)
    - 1 to 1 feature matching with celery and maybe some extras

Sorry guys, I am one man on a very specific mission. All thigns considered v0.2+ is 6 months off.

## License

Copyright 2019- Andrew Evans

Any use, distribution, redistribution, copying, manipulating, or handling of this software may only be done with the
express permission of the copywrite holder. Freedom of access to this software may change at any stated time unless
stated directly in a written contract. Unauthorized use of this software is prohibited without the express consent of
the author. Derived works shall be considered a part of the original work and are subject to the terms of this license.
