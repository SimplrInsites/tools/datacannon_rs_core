//! # Datacannon Core
//!
//! `datacannon_rs_core` is a collection of building blocks for workers and clients in the
//! job queue system.
#![feature(box_into_pin)]
#[macro_use]
extern crate derive_builder;

pub use lapin::BasicProperties as AMQPProperties;
pub use amq_protocol::types::AMQPValue as AmqpValue;
pub use tokio;


pub mod app;
pub mod argparse;
pub mod backend;
pub mod broker;
pub mod config;
pub mod connection;
pub mod events;
pub mod message_protocol;
pub mod nodename;
pub mod serde_utils;
pub mod task;
pub mod message_structure;
pub mod error;
pub mod replication;
pub mod router;
pub mod result;
pub mod time;
