//! Produces event messages to be handled by the system. The event system serves as the maintenance
//! backbone of the system. Messages from the producer must be handled by workers. Messages include
//! requests for status and requests to terminate among others.
//!
//! The Redis producer assumes that redis is the backend or broker and is running on your system.
//! The correct producer should be running for your system.
//!
//! Use the event names constants for standardized name passing. Only use serialized events in the
//! event producer.
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::backend::config::BackendConfig;
use crate::backend::redis::producer::{self, RedisProducer};

/// Producer that takes serialized event messages and names and pushes to the
pub struct RedisEventProducer{
    producer: RedisProducer
}


/// Implementation
impl RedisEventProducer{

    /// Send and event using the underlying redis connection. Events need to be serialized with
    /// a structure containing a type attribute from the event types constant and the event itself.
    ///
    /// Returns a success message containing the number of retries or an error.
    ///
    /// # Arguments
    /// * `retries` - Number of times to retry sending an event
    /// * `routing_key` - Key to publish event to
    /// * `event` - Serialized event object
    pub async fn send_event(
        &mut self, retries: Option<u8>, routing_key: &'static str, event: String) -> Result<u8, ()>{
        let paired_conn = self.producer.get_paired_conn().clone();
        let key_string = routing_key.to_string();
        let result = producer::publish(
            paired_conn, retries, Some(100), key_string, event).await;
        if result.0 {
            Ok(result.1)
        }else{
            Err(())
        }
    }

    /// Create a new producer
    ///
    /// # Arguments
    /// * `backend_config` - Backend Configuration
    pub async fn new(backend_config: BackendConfig) -> RedisEventProducer{
        let producer = RedisProducer::new(backend_config).await;
        RedisEventProducer{
            producer
        }
    }
}


#[cfg(test)]
pub mod test{
    use tokio::runtime::Runtime;

    use crate::backend::config::BackendConfig;
    use crate::events::messages::terminate::Terminate;
    use crate::events::redis_producer::RedisEventProducer;
    use std::sync::Arc;

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .ok().unwrap();
        Arc::new(rt)
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    #[test]
    fn should_create_producer(){
        let rt = get_runtime();
        let config = get_backend_config();
        rt.block_on(async move {
            let _producer = RedisEventProducer::new(config);
        });
    }

    #[test]
    fn should_send_message_to_backend(){
        let rt = get_runtime();
        rt.block_on(async move {
            let config = get_backend_config();
            let mut producer = RedisEventProducer::new(config).await;
            let message = Terminate::new();
            let message_string = serde_json::to_string(&message);
            assert!(message_string.is_ok());
            let routing_key = "test_key";
            let _r = producer.send_event(
                Some(1), routing_key, message_string.ok().unwrap()).await;
        });
    }
}
