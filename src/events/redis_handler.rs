//! Events handler for graceful shutdown the heartbeat controller and more. Events are sent as
//! strings containing a mapping including the event type and potentially a deserializable object.
//!
//! This version relies on redis to handle consumer input. Users should have choices as to which
//! tool will implement their events. This is due to the changing nature of backends and brokers.
//!
//! Redis is a solid choice as the events routing keys should be limited and no special routing
//! is required as of now.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use futures::StreamExt;
use log::debug;
use redis_async::client::PubsubConnection;
use redis_async::resp::FromResp;
use tokio::task::JoinHandle;

use crate::backend::config::BackendConfig;
use crate::backend::redis::consumer::RedisConsumer;
use crate::config::config::CannonConfig;
use crate::events::context::EventContext;

/// Redis based event handler for handling system events through Redis
pub struct RedisEventHandler{
    config: CannonConfig<'static>,
    consumer: RedisConsumer,
    event_context: EventContext,
    delegator: fn(String, EventContext) -> bool
}


/// Implementation of the redis event handler
impl RedisEventHandler {

    /// Get the type of the event from the producer
    pub fn get_event_type() -> &'static str{
        ""
    }

    /// Runs the consumer. Returns an error if the subscription failed.
    ///
    /// # Argument
    /// * `route` - Route to subscribe to
    /// * `conn` - Pubsub connection to use
    /// * `ctx` - Event context containing key application variables
    /// * `delegator` - Custom event handler function
    async fn run_consumer(
        route: &'static str,
        conn: PubsubConnection,
        ctx: EventContext,
        delegator: fn(String, EventContext) -> bool) -> Result<(), ()>{
        debug!("Datacannon :: core :: Subscribing redis consumer to {}", route);
        let sub_result = conn.subscribe(route.clone()).await;
        if sub_result.is_ok() {
            let mut ps_stream= sub_result.ok().unwrap();
            let mut process = true;
            let app_status = ctx.get_app_status();
            while process && app_status.load(Ordering::Relaxed) {
                if let Some(message) = ps_stream.next().await {
                    match message {
                        Ok(message) => {
                            let mstr = String::from_resp(message).unwrap();
                            process = delegator(mstr, ctx.clone());
                        },
                        Err(e) => {
                            eprintln!("ERROR: {}", e);
                        }
                    }
                }
            }
            conn.unsubscribe(route);
            Ok(())
        }else{
            Err(())
        }
    }

    /// Start the event handler
    pub async fn start(&mut self) -> JoinHandle<Result<(), ()>>{
        let ctx = self.config.clone();
        let event_route = ctx.event_routing_key;
        let conn = self.consumer.get_conn();
        let delegator = self.delegator;
        let ctx = self.event_context.clone();
        tokio::spawn(async move{
            RedisEventHandler::run_consumer(event_route, conn, ctx, delegator).await
        })
    }

    /// create a new redis handler.
    ///
    /// # Arguments
    /// * `config` - Configuration for the application
    /// * `app_status` - Overall application status
    /// * `backend_config` - Configuration containing connection information
    /// * `delegator` - Handles message events and returns wehther to continue processing messages.
    pub async fn new(
        config: CannonConfig<'static>,
        app_status: Arc<AtomicBool>,
        backend_config: BackendConfig,
        delegator: fn(String, EventContext) -> bool) -> Result<RedisEventHandler, ()> {
        let consumer = RedisConsumer::new(backend_config).await;
        if consumer.is_ok(){
            let consumer= consumer.ok().unwrap();
            let event_context = EventContext::new(app_status);
            let handler = RedisEventHandler{
                config,
                consumer,
                event_context,
                delegator
            };
            Ok(handler)
        }else{
            Err(())
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use tokio::runtime::Runtime;

    use crate::backend::config::BackendConfig;
    use crate::backend::redis::producer;
    use crate::backend::redis::producer::RedisProducer;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::events::context::EventContext;
    use crate::events::event_types;
    use crate::events::redis_handler::RedisEventHandler;
    use crate::router::router::Routers;

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build().unwrap();
        rt
    }

    fn delegator(s: String, ctx: EventContext) -> bool{
        match s.as_str(){
            event_types::TERMINATE => {
                println!("Received");
                ctx.get_app_status().store(false, Ordering::Relaxed);
            }
            _ =>{
                println!("Wrong Type");
            }
        }
        true
    }

    #[test]
    fn should_shutdown_gracefully(){
        let ctx  = get_config();
        let rt = get_runtime();
        let event_route = ctx.event_routing_key.clone();
        let bfg = get_backend_config();
        let app_status = Arc::new(AtomicBool::new(true));
        let e = event_types::TERMINATE;
        rt.block_on(async move {
            let producer = RedisProducer::new(bfg.clone()).await;
            let pconn = producer.get_paired_conn();
            let handler_result = RedisEventHandler::new(
                ctx, app_status, bfg.clone(), delegator).await;
            assert!(handler_result.is_ok());
            let mut event_handler = handler_result.ok().unwrap();
            let thandle = event_handler.start().await;
            let result = producer::publish(
                pconn.clone(),
                Some(1),
                Some(10),
                event_route.clone().to_string(),
                e.to_string()).await;
            assert!(result.0);
            let tr = thandle.await;
            assert!(tr.is_ok());
        });
    }
}
