//! Termination message telling the receiving nodes to shutdown.
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};
use crate::events::event_types;


/// General termination message
#[derive(Serialize, Deserialize)]
pub struct Terminate{
    name: &'static str
}


/// Terminate implementation
impl Terminate{

    /// Create the new termination message
    #[allow(dead_code)]
    pub fn new() -> Terminate{
        Terminate{
            name: event_types::TERMINATE
        }
    }
}
