//! This set of constants holds the names of events sent over a producer and consumed by an event
//! consumer.
//!
//! ---
//! author: Andrew Evans
//! ---

pub const TERMINATE: &'static str = "TERMINATE";
