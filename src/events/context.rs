//! Event context storing critical application values
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::AtomicBool;

/// Event Context storing critical values from the application
#[derive(Clone, Debug)]
pub struct EventContext{
    app_status: Arc<AtomicBool>
}


/// Implementation of the Event Context
impl EventContext{

    /// Get the application status
    pub fn get_app_status(&self) -> Arc<AtomicBool>{
        self.app_status.clone()
    }

    /// Create a new event context
    ///
    /// # Arguments
    /// * `app_status` - Application status
    pub fn new(app_status: Arc<AtomicBool>) -> EventContext{
        EventContext{
            app_status
        }
    }
}
