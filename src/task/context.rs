//! A task context containing arguments and flow based configuration
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;

use crate::argparse::argtype::ArgType;
use crate::message_protocol::stream::StreamConfig;

/// Task context containing function specific mappings
#[allow(dead_code)]
#[derive(Clone)]
pub struct TaskContext {
    args: Option<Vec<ArgType>>,
    kwargs: Option<HashMap<String, ArgType>>,
    stream_config: Option<StreamConfig>,
}


/// Task Context implementation
impl TaskContext {
    /// Get the arguments from the context
    pub fn get_args(&mut self) -> Option<Vec<ArgType>> {
        self.args.clone()
    }

    /// Get the kwargs for the task
    pub fn get_kwargs(&mut self) -> Option<HashMap<String, ArgType>> {
        self.kwargs.clone()
    }

    /// Get the task stream config
    pub fn get_stream_config(&mut self) -> Option<StreamConfig> {
        self.stream_config.clone()
    }

    /// Create a new task context
    ///
    /// # Arguments
    /// * `args` - Arguments for the function
    /// * `kwargs` - Mapped arguments for the function
    /// * `stream_config` - Stream control
    #[allow(dead_code)]
    pub fn new(
        args: Option<Vec<ArgType>>,
        kwargs: Option<HashMap<String, ArgType>>,
        stream_config: Option<StreamConfig>) -> TaskContext {
        TaskContext {
            args,
            kwargs,
            stream_config,
        }
    }
}
