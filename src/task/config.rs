//! Basic configuration for the tasks
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use serde::{self, Deserialize, Serialize};
use uuid::Uuid;

use crate::argparse::argtype::ArgType;
use crate::config::config::CannonConfig;
use crate::message_protocol::headers::{Headers, TimeLimit};
use crate::message_protocol::message::Message;
use crate::message_protocol::properties::Properties;
use crate::message_protocol::stream::StreamConfig;

/// Exchange types
pub mod exchange_types{
    pub const FANOUT: &'static str = "FANOUT";
    pub const HEADERS: &'static str= "HEADERS";
    pub const DIRECT:  &'static str= "DIRECT";
    pub const TOPIC: &'static str = "TOPIC";
}


/// Task Configuration
///
/// # Arguments
/// * `task_name` - The task name string
/// * `reply_to` - Queue to reply to
/// * `correlation_id` - The unique correlation id
/// * `expires` - Expiration time for the task
/// * `priority` - Priority for the task
/// * `time_limit` - Time limit for the task
/// * `soft_time_limit` - Soft time limit for the Task
/// * `eta` - Estimated time of arrival
/// * `retries` - Number of retries
#[derive(Clone, Serialize, Builder, Default, Deserialize, Debug)]
#[builder(setter(into))]
pub struct TaskConfig{
    broker: String,
    task_name: String,
    args: Vec<ArgType>,
    kwargs: HashMap<String, ArgType>,
    reply_to: String,
    parent_id: String,
    correlation_id: String,
    result_expires: u64,
    priority: i8,
    time_limit: u64,
    soft_time_limit: u64,
    eta: u64,
    retries: u8,
    lang: String,
    shadow: String,
    exchange: Option<String>,
    exchange_type: Option<String>,
    routing_key: Option<String>
}


/// Implementation for the task config
impl TaskConfig{

    /// Set the parent id
    ///
    /// # Arguments
    /// * `parent_id` - Parent id to set
    pub fn set_parent_id(&mut self, parent_id: String){
        self.parent_id = parent_id;
    }

    /// Get the parent id String
    pub fn get_parent_id(&self) -> String{
        self.parent_id.clone()
    }

    /// Get the exchange type option
    pub fn get_exchange_type(&self) -> Option<String>{self.exchange_type.clone()}

    /// Get the routing key option
    pub fn get_routing_key(&self) -> Option<String> {self.routing_key.clone()}

    /// Get the exchange option
    pub fn get_exchange(&self) -> Option<String> { self.exchange.clone() }

    /// Get the number of retries `std::i8`
    pub fn get_retries(&self) -> u8{
        self.retries.clone()
    }

    /// Get the eta date as a u64
    pub fn get_eta(&self) -> u64{
        self.eta.clone()
    }

    /// Get the soft time limit `std::u64`
    pub fn get_soft_time_limit(&self) -> u64{
        self.soft_time_limit.clone()
    }

    /// Get the time limit `std::u64`
    pub fn get_time_limit(&self) -> u64{
        self.time_limit.clone()
    }

    /// Get the priority `std::i8`
    pub fn get_priority(&self) -> i8{
        self.priority.clone()
    }

    /// Get the time to live
    pub fn get_result_expires(&self) -> u64{
        self.result_expires.clone()
    }

    /// Get the correlation id `std::String`
    pub fn get_correlation_id(&self) -> &String{
        &self.correlation_id
    }

    /// Get the reply to queue `std::String`
    pub fn get_reply_to(&self) -> &String{
        &self.reply_to
    }

    /// Get the args map of `HashMap<String,crate::argparse::argtype::ArgType>`
    pub fn get_kwargs(&self) -> HashMap<String, ArgType>{
        self.kwargs.clone()
    }

    /// Get the `crate::argparse::args::Args`
    pub fn get_args(&self) -> Vec<ArgType>{
        self.args.clone()
    }

    /// Get the task name `std::String`
    pub fn get_task_name(&self) -> &String {
        &self.task_name
    }

    pub fn get_task_lang(&self) -> &String{
        &self.lang
    }

    /// Get the logging nickname `std::String`
    pub fn get_shadow(&self) -> String{
        self.shadow.clone()
    }

    /// Get the broker type name
    pub fn get_broker(&self) -> String{
        self.broker.clone()
    }

    /// Convert to a message body
    ///
    /// # Arguments
    /// * `accept_content` - Content type to accept
    /// * `encoding_type` - Content encoding type
    /// * `stream_config` - Message body to add to the message
    /// * `root_id` - Originating id for the chain
    pub fn to_amqp_message(&self, accept_content: &str, encoding_type: &str, stream_config: Option<StreamConfig>, root_id: Option<String>) -> Message{
        let properties = Properties::new(
            self.correlation_id.clone().as_str(),
            accept_content.clone(),
            encoding_type.clone(),
            Some(self.reply_to.clone().as_str()));
        let mut root = self.correlation_id.clone();
        if root_id.is_some(){
            root = root_id.unwrap();
        }
        let mut mbody = StreamConfig::new(None,None);
        if stream_config.is_some(){
            mbody = stream_config.unwrap();
        }
        let mut headers = Headers::new(self.lang.as_str(), self.broker.as_str(), self.task_name.as_str(), self.correlation_id.as_str(), root.as_str());
        headers.parent_id = Some(self.parent_id.clone());
        headers.broker = self.broker.clone();
        headers.shadow = Some(self.shadow.clone());
        headers.eta = Some(self.eta.clone());
        headers.expires = Some(self.result_expires.clone());
        headers.lang = self.lang.clone();
        headers.retries = Some(self.retries.clone() as i8);
        headers.timelimit = Some(TimeLimit{ soft: self.soft_time_limit, hard: self.time_limit });
        let m = Message::new(properties, headers, mbody, self.args.clone(), self.kwargs.clone());
        m
    }

    /// Create a new configuration
    ///
    /// # Arguments
    /// * `config` - Cannon Configuration
    /// * `broker_type` - Name of the broker type
    /// * `task_name` - Name of the task
    /// * `exchange` - Name of the exchange to send to
    /// * `routing_key` - Name of the routing key to send to
    /// * `parent_id` - Parent id which will be generated if not set and should be overwritable
    /// * `args` - The `crate::argparse::args::Args` for the task
    /// * `kwargs` - The arg HashMap for the task
    /// * `reply_to` - Queue to reply to
    /// * `correlation_id` - The correlation id
    /// * `result_expires` - Result expiration time to live
    /// * `priority` - Result priority
    /// * `time_limit` - Hard time limit for the task (TTL)
    /// * `soft_time_limit` - Soft time limit ttl
    /// * `eta` - Estimated time of arrival
    /// * `retries` - Number of times to retry the task
    /// * `lang` - Language for the task
    pub fn new(
        config: CannonConfig<'static>,
        broker_type: String,
        task_name: String,
        exchange: Option<String>,
        exchange_type: Option<String>,
        routing_key: Option<String>,
        parent_id: Option<String>,
        args: Option<Vec<ArgType>>,
        kwargs: Option<HashMap<String, ArgType>>,
        reply_to: Option<String>,
        correlation_id: Option<String>,
        result_expires: Option<u64>,
        priority: Option<i8>,
        time_limit: Option<u64>,
        soft_time_limit: Option<u64>,
        eta: u64,
        retries: Option<u8>,
        lang: Option<String>,
        shadow: Option<String>) -> TaskConfig{

        let mut parent = format!("{}", Uuid::new_v4());
        if parent_id.is_some(){
            parent = parent_id.unwrap();
        }
        let mut targs= Vec::<ArgType>::new();
        if args.is_some(){
            targs = args.unwrap();
        }

        let mut tkwargs = HashMap::<String, ArgType>::new();
        if kwargs.is_some(){
            tkwargs = kwargs.unwrap();
        }

        let mut corrid = format!("{}", Uuid::new_v4());
        if correlation_id.is_some(){
            corrid = correlation_id.clone().unwrap();
        }

        let mut reply = format!("{}", Uuid::new_v4());
        if reply_to.is_none() && correlation_id.is_some(){
            reply = corrid.clone();
        }else if reply_to.is_some(){
            reply = reply_to.unwrap();
        }

        let mut texpires = 36000000;
        if result_expires.is_some(){
            texpires = result_expires.unwrap();
        }

        let mut tpriority = config.task_default_priority.clone();
        if priority.is_some(){
            tpriority = priority.unwrap();
        }

        let mut ttime_limit = 36000000;
        if time_limit.is_some(){
            ttime_limit = time_limit.unwrap();
        }

        let mut tsoft_time_limit = 36000000;
        if soft_time_limit.is_some(){
            tsoft_time_limit = soft_time_limit.unwrap();
        }

        let mut tretries = config.task_retries.clone();
        if retries.is_some(){
            tretries = retries.unwrap();
        }

        let mut tlang = config.default_lang.to_string();
        if lang.is_some(){
            tlang = lang.unwrap();
        }

        let mut shadow_name = task_name.clone();
        if shadow.is_some(){
            shadow_name = shadow.unwrap();
        }

        TaskConfig{
            broker: broker_type,
            task_name,
            args: targs,
            kwargs: tkwargs,
            reply_to: reply,
            parent_id: parent,
            correlation_id: corrid,
            result_expires: texpires,
            priority: tpriority,
            time_limit: ttime_limit,
            soft_time_limit: tsoft_time_limit,
            eta,
            retries: tretries,
            lang: tlang,
            shadow: shadow_name,
            exchange,
            exchange_type,
            routing_key
        }
    }
}
