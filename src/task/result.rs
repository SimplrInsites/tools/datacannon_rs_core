//! A return type to standardize execution. Results should be standardized to ensure
//! adequate functionality.
//!
//! The return type contains a results mapping which will serve as the arguments to
//! a chain or chord.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::argparse::argtype::ArgType;
use crate::message_protocol::stream::StreamConfig;
use crate::task::config::TaskConfig;

/// The result structure containing result values
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Response{
    results: HashMap<String, ArgType>
}


/// The task response wrapping the general response and typically created by the thread pools
/// when pushing to a backend handler
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TaskResponse{
    task: TaskConfig,
    response: Response,
    stream_config: StreamConfig,
    status: bool,
    error: Option<String>,
    remaining_attempts: u8
}


/// Task response implementation
impl TaskResponse{

    /// Get a reference to teh underlying stream configuration
    pub fn get_stream_config(&self) -> &StreamConfig{
        &self.stream_config
    }

    /// Obtain a reference to the response object
    pub fn get_response(&self) -> &Response{
        &self.response
    }

    /// Obtain a reference to the underlying task
    pub fn get_task(&self) -> &TaskConfig{
        &self.task
    }

    ///Obtain the remaining send attempts for this message
    pub fn get_remaining_retries(&mut self) -> u8{
        self.remaining_attempts
    }

    /// Decrements the number of retries remaining
    pub fn decrement_retries(&mut self){
        self.remaining_attempts -= 1;
    }

    /// Check whether the task completed succesfully
    pub fn is_ok(&self) -> bool{
        self.status.clone()
    }

    /// Get the error
    pub fn get_error(&self) -> Option<String>{
        self.error.clone()
    }

    /// Create a new Task Response
    ///
    /// # Arguments
    /// * `task` - The task configuration
    /// * `response` - Associated response object
    /// * `stream_config` - Stream configuration
    /// * `status` - Whether the task succeeded or failed
    /// * `error` - Any error if it occurred
    pub fn new(task: TaskConfig, response: Response, stream_config: StreamConfig, status: bool, error: Option<String>) -> TaskResponse{
        TaskResponse{
            task: task,
            response: response,
            stream_config: stream_config,
            status: status,
            error: error,
            remaining_attempts: 3
        }
    }
}


/// Implementation of the results
impl Response{

    /// Add a result to underlying results object
    ///
    /// # Arguments
    /// * `key` - Key name for the result
    /// * `result` - Result value
    pub fn add_result(&mut self, key: String, result: ArgType) {
        self.results.insert(key, result);
    }

    /// Get the results from teh program
    ///
    /// # Arguments
    /// * `key` - Key to obtain the result with
    pub fn get_result(&self, key: &str) -> Option<&ArgType>{
        self.results.get(key)
    }

    /// Get the underlying arguments map
    pub fn get_map(&self) -> HashMap<String, ArgType>{
        self.results.clone()
    }

    /// Create a new results object
    pub fn new() -> Response{
        Response{
            results: HashMap::<String, ArgType>::new()
        }
    }
}
