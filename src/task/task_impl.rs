//! Task implemntation. The task should
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::task::result::Response;

/// Generic base trait for all thread ready functions
pub trait FnBoxGeneric<T> {
    fn call_box(self: Box<Self>) -> Option<T>;
}


/// Implementation for no return
impl <F: FnOnce() -> ()> FnBoxGeneric<()> for F {
    fn call_box(self: Box<F>) -> Option<()> {
        Some((*self)())
    }
}


/// Implementation for a return containing a Response
impl <F: FnOnce() -> Response> FnBoxGeneric<Response> for F {
    fn call_box(self: Box<F>) -> Option<Response> {
        Some((*self)())
    }
}


/// Boxed wrapper to peg the function to memory
pub type ReturnableThunk<T> = Box<dyn FnBoxGeneric<T> + Send>;


#[cfg(test)]
mod test{
    use std::collections::HashMap;
    use std::mem::MaybeUninit;
    use std::thread;

    use tokio::runtime::Builder;

    use crate::argparse::argtype::ArgType;
    use crate::message_protocol::stream::StreamConfig;
    use crate::task::result::Response;
    use crate::task::task_impl::ReturnableThunk;

    fn test_it(args: Option<Vec<ArgType>>, kwargs: Option<HashMap<&'static str, ArgType>>, sc: Option<StreamConfig>) -> Response {
        if args.is_some(){
            println!("Received Args");
        }
        if kwargs.is_some(){
            println!("Received Kwargs");
        }
        if sc.is_some(){
            println!("Received kwargs");
        }
        let mut r = Response::new();
        r.add_result("test".to_string(), ArgType::Bool(true));
        r
    }

    fn test_it_b(args: Option<Vec<ArgType>>, kwargs: Option<HashMap<&'static str, ArgType>>, sc: Option<StreamConfig>) {
        if args.is_some(){
            println!("Received Args");
        }
        if kwargs.is_some(){
            println!("Received Kwargs");
        }
        if sc.is_some(){
            println!("Received kwargs");
        }
    }

    #[test]
    fn should_create_task_out_of_function(){
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let f = move || -> Response {
            test_it(args, kwargs, sc)
        };
        let ipl: ReturnableThunk<Response> = Box::new( f);
        let r = ipl.call_box();
        assert!(r.is_some());
        assert!(r.unwrap().get_result("test").is_some());
    }

    #[test]
    fn should_run_no_response_from_new_thread(){
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let th = test_it_b;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let mut reg: HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> ()> = HashMap::new();
        reg.insert("test", th);
        let f = reg.get("test").unwrap().clone();
        let eth: ReturnableThunk<()> = Box::new(move|| -> () {
            f(args, kwargs, sc);
        });
        let handle = thread::spawn(move||{
            eth.call_box()
        });
        let r = handle.join();
        assert!(r.is_ok());
    }

    #[test]
    fn should_run_from_new_thread(){
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let th = test_it;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let mut reg: HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> Response> = HashMap::new();
        reg.insert("test", th);
        let f = reg.get("test").unwrap().clone();
        let eth: ReturnableThunk<Response> = Box::new(move|| -> Response {
            f(args, kwargs, sc)
        });
        let handle = thread::spawn(move||{
            eth.call_box()
        });
        let r = handle.join();
        assert!(r.is_ok());
        assert!(r.ok().unwrap().unwrap().get_result("test").is_some());
    }

    #[test]
    fn should_run_from_thread(){
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let th = test_it;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let mut reg: HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> Response> = HashMap::new();
        reg.insert("test", th);
        let mut  f = reg.get("test").unwrap().clone();
        let mut  eth: ReturnableThunk<Response> = Box::new(move|| -> Response {
            f(args, kwargs, sc)
        });
        let mut r = eth.call_box();
        assert!(r.is_some());
        assert!(r.unwrap().get_result("test").is_some());
        f = reg.get("test").unwrap().clone();
        eth = Box::new(move|| -> Response {
            f(None, None, None)
        });
        r = eth.call_box();
        assert!(r.is_some());
        assert!(r.unwrap().get_result("test").is_some());
    }

    #[test]
    fn should_run_in_future(){
        let rt = Builder::new_multi_thread().enable_all().worker_threads(4).build().ok().unwrap();
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let th = test_it;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let mut reg: HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> Response> = HashMap::new();
        reg.insert("test", th);
        let f = reg.get("test").unwrap().clone();
        let eth: ReturnableThunk<Response> = Box::new(move|| -> Response {
            f(args, kwargs, sc)
        });
        let asl = async{
            eth.call_box()
        };
        let r = rt.block_on(async move{
            asl.await
        });
        assert!(r.is_some());
        assert!(r.unwrap().get_result("test").is_some());
        println!();
    }

    #[test]
    fn should_run_from_maybe_uninit(){
        let mut maybe = MaybeUninit::< HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> Response> >::uninit();
        let rt = Builder::new_multi_thread().enable_all().worker_threads(4).build().ok().unwrap();
        let args: Option<Vec<ArgType>> = None;
        let kwargs: Option<HashMap<&'static str, ArgType>> = None;
        let th = test_it;
        let sc: Option<StreamConfig> = Some(StreamConfig::new(None,None));
        let reg: HashMap<&'static str, fn(Option<Vec<ArgType>>, Option<HashMap<&'static str, ArgType>>, Option<StreamConfig>) -> Response> = HashMap::new();
        unsafe{
            let mptr = maybe.as_mut_ptr();
            mptr.write(reg);
        };
        let mreg = unsafe{
            let mptr = maybe.as_mut_ptr();
            &mut *mptr
        };
        mreg.insert("test", th);
        let f = mreg.get("test").unwrap().clone();
        let eth: ReturnableThunk<Response> = Box::new(move|| -> Response {
            f(args, kwargs, sc)
        });
        let asl = async{
            eth.call_box()
        };
        let r = rt.block_on(async move{
            asl.await
        });
        assert!(r.is_some());
        assert!(r.unwrap().get_result("test").is_some());
        println!();
    }
}
