//! Holds a task error which stores the error and any other value
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::message_protocol::stream::StreamConfig;
use crate::task::config::TaskConfig;

/// Stores task error information.
#[derive(Clone, Debug)]
pub struct TaskError{
    task_config: TaskConfig,
    stream_config: StreamConfig,
    error: String
}


/// Implementation of the task error
impl TaskError{

    /// Get the task configuration
    pub fn get_task_config(&mut self) -> &TaskConfig{
        &self.task_config
    }

    /// Get the stream configuration
    pub fn get_stream_config(&mut self) -> &StreamConfig{
        &self.stream_config
    }

    /// Get the error for the configuration
    pub fn get_error(&mut self) -> &str{
        self.error.as_str()
    }

    /// Create a new Task Error
    pub fn new(task_config: TaskConfig, stream_config: StreamConfig, error: String) -> TaskError{
        TaskError{
            task_config,
            stream_config,
            error
        }
    }
}
