//! Store application information to setup existing queues
//!
//! ---
//! author: Andrew Evans
//! ---

use amq_protocol_types::ShortUInt;

use crate::connection::amqp::connection_inf::AMQPConnectionInf;
use crate::message_structure::queue_trait::QueueHandler;
use crate::replication::replication::HAPolicy;

/// AMQP Queue
#[derive(Clone, Debug)]
pub struct AMQPQueue {
    name: String,
    exchange: Option<String>,
    routing_key: Option<String>,
    max_priority: i8,
    ha_policy: HAPolicy,
    conn_inf: AMQPConnectionInf,
    is_durable: bool,
    prefetch_count: ShortUInt
}


/// Implements the base queue functions
impl QueueHandler for AMQPQueue{
    /// Get the queue name `std::&str::&str`
    fn get_name(&self) -> &str {
        self.name.as_str()
    }

    /// Get a cloned copy of the name
    fn clone_name(&self) -> String{
        self.name.clone()
    }
}


/// AMQPQueue implementation
impl AMQPQueue {

    /// Whether the queue is durable
    pub fn is_durable(&self) -> bool{
        self.is_durable
    }

    /// Get the prefetch count
    pub fn get_prefetch_count(&self) -> ShortUInt{
        self.prefetch_count.clone()
    }

    /// Sets the prefetch count for the queue
    ///
    /// # Arguments
    /// * `prefetch_count` - Prefetch count for the channel
    pub fn set_prefetch_count(&mut self, prefetch_count: ShortUInt){
        self.prefetch_count = prefetch_count;
    }

    /// Create a new AMQP queue
    ///
    /// # Arguments
    /// * `name` - Name of the queue
    /// * `exchange` - The exchange name
    /// * `routing_key` - Routing key for queue
    /// * `max_priority` - The maximum priority
    /// * `ha_policy` - Availability policy
    /// * `is_durable` - Whether the queue is durable
    /// * `conn_inf` - The connection information
    pub fn new(name: String,
           exchange: Option<String>,
           routing_key: Option<String>,
           max_priority: i8,
           ha_policy: HAPolicy,
           is_durable: bool,
           conn_inf: AMQPConnectionInf) -> AMQPQueue{
        AMQPQueue{
            name: name.to_string(),
            exchange: exchange,
            routing_key: routing_key,
            max_priority: max_priority,
            ha_policy: ha_policy,
            conn_inf: conn_inf,
            is_durable: is_durable,
            prefetch_count: 1
        }
    }
}
