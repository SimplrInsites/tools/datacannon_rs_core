//! Manages rabbitMQ exchanges
//!
//! ---
//! author: Andrew Evans
//!

use lapin::ExchangeKind;

///Exchange structure
#[derive(Clone, Debug)]
pub struct Exchange{
    name: String,
    exchange_type: ExchangeKind,
}


/// The exchange implementation
impl Exchange{

    /// Get the name of the exchange
    pub fn get_name(&self) -> &str{
        self.name.as_str()
    }

    /// Get the exchange kind
    pub fn get_exchange_type(&self) -> ExchangeKind{
        self.exchange_type.clone()
    }

    /// Get a new Exchange
    pub fn new(name: String, exchange_type: ExchangeKind) -> Exchange{
        Exchange{
            name: name.to_string(),
            exchange_type: exchange_type
        }
    }
}


/// Convert a string to an exchange Kind
///
/// # Arguments
/// * `exchange` - The exchange
pub fn convert_to_kind(exchange: String) -> ExchangeKind{
    match exchange.to_uppercase().as_str(){
        "DIRECT" => ExchangeKind::Direct,
        "FANOUT" => ExchangeKind::Fanout,
        "TOPIC" => ExchangeKind::Topic,
        "HEADERS" => ExchangeKind::Headers,
        _ => ExchangeKind::Custom(exchange.clone())
    }
}