//! General Queues implementation.
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::message_structure::amqp::queue::AMQPQueue;

/// Stores queues for the broker
///
/// # Arguments
/// * `AMQPQueue` - Wrapper for `crate::message_structure::amqp::queue::AMQPQueue`
#[derive(Clone, Debug)]
pub enum GenericQueue {
    AMQPQueue(AMQPQueue)
}
