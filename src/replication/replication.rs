//! Replication policy structure to allow for use of different brokers
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::replication::rabbitmq::RabbitMQHAPolicy;

/// HA Policy enum storing relevant policy
#[derive(Clone, Debug)]
pub enum HAPolicy{
    RabbitMQ(RabbitMQHAPolicy),
}
