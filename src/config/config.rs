//! General configuration for the framework
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use lapin::ExchangeKind;
use num_cpus;

use crate::argparse::argtype::ArgType;
use crate::backend::config::BackendConfig;
use crate::broker::broker_type;
use crate::connection::connection::ConnectionConfig;
use crate::replication::replication::HAPolicy;
use crate::router::router::Routers;

/// Queue persistance type
///
/// # Arguments
/// * `PERSISTENT` - When possible, queue will persist
/// * `NONPERSISTENT` - When possible, queue will drop
#[derive(Clone, Debug)]
pub enum QueuePersistenceType{
    PERSISTENT,
    NONPERSISTENT
}


/// Backend types available
///
/// # Arguments
/// * `REDIS` - Uses Redis
#[derive(Clone, Debug)]
pub enum BackendType{
    REDIS
}


/// Broker type
///
/// # Arguments
/// * `RABBITMQ` - Use RabbitMQ
#[derive(Clone, Debug)]
pub enum BrokerType{
    RABBITMQ,
}


/// Broker type implmentation
impl BrokerType{

    /// Convert to string
    pub fn to_string(&self) -> &'static str{
        match self{
            BrokerType::RABBITMQ => {
                broker_type::RABBITMQ
            }
        }
    }
}


/// Admin information
///
/// # Arguments
/// * `name` - Administrator name
/// * `email` - Administrator email
#[derive(Clone, Debug)]
pub struct Admin<'a>{
    name: &'a str,
    email: &'a str,
}


/// Configuration for the application with all public variables
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct CannonConfig<'a>{
    pub connection_inf: ConnectionConfig,
    pub event_backend_type: BackendType,
    pub event_backend: BackendConfig,
    pub default_backend: BackendType,
    pub routers: Routers,
    pub cache_backend: Option<BackendConfig>,
    pub send_events: bool,
    pub default_exchange: &'a str,
    pub default_exchange_type: ExchangeKind,
    pub default_queue: &'a str,
    pub event_routing_key: &'a str,
    pub result_exchange: &'a str,
    pub accept_content: &'a str,
    pub worker_prefetch_multiplier: i8,
    pub default_delivery_mode: QueuePersistenceType,
    pub default_routing_key: &'a str,
    pub broker_connection_timeout: i64,
    pub broker_connection_max_retries: i64,
    pub acks_late: bool,
    pub task_result_expires: i64,
    pub ignore_result: bool,
    pub max_cached_results: i32,
    pub result_persistent: QueuePersistenceType,
    pub result_serializer: &'a str,
    pub database_engine_options: Option<HashMap<&'a str, &'a str>>,
    pub num_broker_connections: u32,
    pub num_broker_channels: u32,
    pub num_broker_threads: u32,
    pub num_backend_connections: u32,
    pub num_backend_channels: u32,
    pub num_backend_threads: u32,
    pub ha_policy: Option<HAPolicy>,
    pub create_missing_queues: bool,
    pub broker_transport_options: Option<HashMap<&'a str, ArgType>>,
    pub task_queue_max_priority: Option<i8>,
    pub task_default_priority: i8,
    pub maximum_allowed_failures: u8,
    pub maximum_allowed_failures_per_n_calls: u8,
    pub task_retries: u8,
    pub default_lang: &'a str,
    pub app_lang: &'a str,
    pub encoding_type: &'a str,
    pub message_size: usize,
    pub drop_queues_on_close: bool,
    pub purge_queues_on_start: bool,
    pub drop_exchanges_on_close: bool,
    pub prefetch_limit: usize,
    pub max_concurrent_tasks:usize,
    pub consumers_per_queue: usize
}


/// Implementation of Celery configuration
impl <'a> CannonConfig<'a>{

    /// Create a new configuration
    pub fn new(
        conn_inf: ConnectionConfig,
        event_backend_type: BackendType,
        event_backend: BackendConfig,
        routers: Routers) -> CannonConfig<'a>{
        CannonConfig{
            connection_inf: conn_inf,
            event_backend_type,
            event_backend,
            default_backend: BackendType::REDIS,
            cache_backend: None,
            send_events: false,
            routers,
            default_exchange: "cannon",
            default_exchange_type: ExchangeKind::Direct,
            default_queue: "cannon",
            event_routing_key: "cannonevent",
            result_exchange: "cannonresult",
            accept_content: "application/json",
            worker_prefetch_multiplier: 4,
            default_delivery_mode: QueuePersistenceType::PERSISTENT,
            default_routing_key: "cannon",
            broker_connection_timeout: 5000,
            broker_connection_max_retries: 1000,
            acks_late: true,
            task_result_expires: 600000,
            ignore_result: false,
            max_cached_results: 100,
            result_persistent: QueuePersistenceType::NONPERSISTENT,
            result_serializer: "json",
            database_engine_options: None,
            num_broker_channels: num_cpus::get() as u32,
            num_broker_threads: (num_cpus::get()/2) as u32,
            num_broker_connections:  (num_cpus::get()/2) as u32,
            num_backend_connections:  (num_cpus::get()/2) as u32,
            num_backend_channels: num_cpus::get() as u32,
            num_backend_threads: (num_cpus::get()/2) as u32,
            ha_policy: None,
            create_missing_queues: true,
            broker_transport_options: None,
            task_queue_max_priority: None,
            task_default_priority: 0,
            maximum_allowed_failures: 2,
            maximum_allowed_failures_per_n_calls: 10,
            task_retries: 3,
            default_lang: "rs",
            app_lang: "rs",
            encoding_type: "utf-8",
            message_size: 2048,
            drop_queues_on_close: false,
            purge_queues_on_start: false,
            drop_exchanges_on_close: false,
            prefetch_limit: 10,
            max_concurrent_tasks: 2500,
            consumers_per_queue: 10
        }
    }
}


#[cfg(test)]
mod tests {
    use std::env;

    use crate::connection::amqp::connection_inf::AMQPConnectionInf;

    use super::*;

    #[test]
    fn should_create_a_configuration(){
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let broker_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let b = BackendConfig{
            url: "fake",
            username: None,
            password: None,
            transport_options: None,
        };
        let r  = Routers::new();
        let c = CannonConfig::new(
            ConnectionConfig::RabbitMQ(broker_conf), BackendType::REDIS, b,r);
        let conn_inf = c.connection_inf;
        if let ConnectionConfig::RabbitMQ(conn_inf) = conn_inf {
            let url = conn_inf.to_url();
            assert!(url.eq("amqp://dev:rtp*4500@127.0.0.1:5672/test"))
        }
    }
}