//! A list of available brokers as strings for use in message passing
//!
//! ---
//! author: Andrew Evans
//! ---

pub const RABBITMQ: &'static str = "RABBITMQ";
