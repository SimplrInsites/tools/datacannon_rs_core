//! Asynchronous rabbit mq broker utilties. This accompanies the asynch. The utilities take a
//! reference to the existing pool and perform each of the functions in the regular pool.
//!
//! ---
//! author: Andrew Evans
//! ---

use amq_protocol_types::FieldTable;
use lapin::Consumer;
use lapin::options::*;

use crate::broker::amqp::broker_utils;
use crate::broker::amqp::rabbitmq::RabbitMQBroker;
use crate::broker::message::configs::exchange_create::ExchangeCreate;
use crate::broker::message::configs::queue_bind::QueueBind;
use crate::broker::message::configs::queue_declare::CreateQueue;
use crate::broker::message::configs::queue_drop::DropQueue;
use crate::broker::message::configs::queue_purge::PurgeQueue;
use crate::error::exchange_error::ExchangeError;
use crate::error::queue_error::QueueError;
use crate::error::setup_error::SetupError;
use crate::message_structure::amqp::queue::AMQPQueue;
use crate::message_structure::queue_trait::QueueHandler;
use crate::message_structure::queues::GenericQueue;

/// Get the consumer asynchronously
///
/// # Arguments
/// * `broker` - The RabbitMQBroker
/// * `queue` - The queue name
/// * `consumer_tag` - Identifiable consumer tag
pub async fn get_consumer(broker: &mut RabbitMQBroker, queue: String, consumer_tag: String) -> Result<Consumer, ()>{
    let sema = broker.get_channel_semaphore();
    let permit = sema.acquire().await.unwrap();
    permit.forget();
    let ch_opt = broker.get_channel().await;
    if ch_opt.is_some(){
        let ch = ch_opt.unwrap();
        let consumer_opt = ch.basic_consume(&queue, &consumer_tag, BasicConsumeOptions::default(), FieldTable::default()).await;
        let _r = broker.release_channel(ch);
        sema.add_permits(1);
        if consumer_opt.is_ok(){
            Ok(consumer_opt.unwrap())
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Drop the related queue
///
/// # Argument
/// * `queue` - The AMQP queue
pub async fn drop_queue(broker: &mut RabbitMQBroker, queue: AMQPQueue) -> Result<bool, QueueError>{
    let sema = broker.get_channel_semaphore();
    let permit = sema.acquire().await.unwrap();
    permit.forget();
    let ch_opt = broker.get_channel().await;
    if ch_opt.is_some() {
        let ch = ch_opt.unwrap();
        let dq = DropQueue{
            action_id: 0,
            queue: queue.get_name().to_string(),
            nowait: false
        };
        let r = broker_utils::drop_queue(&ch, dq).await;
        let _r = broker.release_channel(ch);
        sema.add_permits(1);
        r
    }else{
        sema.add_permits(1);
        Err(QueueError)
    }
}


/// Bind queues to the exchanges
///
/// # Arguments
/// * `broker` - The AMQP broker reference
pub async fn bind_queues(broker: &mut RabbitMQBroker) -> Result<bool, QueueError>{
    let broker_routers = broker.get_routers();
    let routers = broker_routers.match_routers(".*");
    for i in 0..routers.len() {
        let router_opt = routers.get(i).unwrap();
        let router = router_opt.unwrap().clone();
        let exc = router.get_exchange().unwrap().clone();
        let rkey = router.get_routing_key().clone();
        let queues = router.get_queues().clone();
        let sema = broker.get_channel_semaphore().clone();
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = broker.get_channel().await;
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            for j in 0..queues.len() {
                let q = queues.get(j).unwrap();
                if let GenericQueue::AMQPQueue(q) = q {
                    let queue_bind = QueueBind {
                        action_id: 0,
                        exchange: exc.get_name().to_string(),
                        routing_key: rkey.to_string(),
                        nowait: false,
                        queue: q.get_name().to_string()
                    };
                    let _r = broker_utils::bind_queue(&ch, queue_bind).await;
                }
            }
            broker.release_channel(ch).await
        }
        sema.add_permits(1);
    }
    Ok(true)
}


/// Setup the exchanges
///
/// # Arguments
/// * `broker` - The AMQP broker reference
pub async fn setup_exchanges(broker: &mut RabbitMQBroker) -> Result<bool, ExchangeError>{
    let sema = broker.get_channel_semaphore();
    let permit = sema.acquire().await.unwrap();
    permit.forget();
    let mut is_completed: bool = true;
    let ch_opt = broker.get_channel().await;
    if ch_opt.is_some() {
        let ch = ch_opt.unwrap();
        let broker_routers = broker.get_routers();
        let exchange_names = broker_utils::get_exchange_names(broker_routers);
        for (name, exchange_type) in &exchange_names {
            let create_args = ExchangeCreate {
                action_id: 0,
                durable: true,
                exchange: name.to_string(),
                exchange_type: exchange_type.clone(),
                nowait: false
            };
            let _r = broker_utils::create_exchange(&ch, create_args).await;
        };
        let _r = broker.release_channel(ch);
    }else{
        is_completed = false;
    }
    sema.add_permits(1);
    if is_completed {
        Ok(true)
    } else {
        Err(ExchangeError)
    }
}


/// Purge a given queue
///
/// # Arguments
/// * `broker` - The broker
/// * `queue` - The AMQP queue to purge from
pub async fn purge_queue(broker: &mut RabbitMQBroker, queue: AMQPQueue) -> Result<bool, QueueError>{
    let sema = broker.get_channel_semaphore();
    let permit = sema.acquire().await.unwrap();
    permit.forget();
    let ch_opt = broker.get_channel().await;
    if ch_opt.is_some() {
        let ch = ch_opt.unwrap();
        let pq = PurgeQueue {
            action_id: 0,
            queue: queue.get_name().to_string(),
            nowait: false
        };
        let r = broker_utils::purge_queue(&ch, pq).await;
        let _r = broker.release_channel(ch);
        sema.add_permits(1);
        r
    }else{
        sema.add_permits(1);
        Err(QueueError)
    }
}


/// Setup the broker. Creates queues and exchanges as required. Then creates the futures
///
/// # Arguments
/// * `broker` - The rabbitmq broker to use
pub async fn setup_queues(broker: &mut RabbitMQBroker) -> Result<bool, QueueError>{
    let mut r: Result<bool, QueueError> = Ok(true);
    let sema_arc = broker.get_channel_semaphore();
    let permit = sema_arc.acquire().await.unwrap();
    permit.forget();
    let ch_opt = broker.get_channel().await;
    if ch_opt.is_some() {
        let ch = ch_opt.unwrap();
        let broker_routers = broker.get_routers();
        let routers = broker_routers.match_routers(".*");
        for i in 0..routers.len() {
            let router_opt = routers.get(i).unwrap();
            let router = router_opt.unwrap().clone();
            let queues = router.get_queues().clone();
            for j in 0..queues.len() {
                let q = queues.get(j).unwrap();
                let mut is_durable = false;
                let mut queue_name = "";
                if let GenericQueue::AMQPQueue(q) = q {
                    is_durable = q.is_durable();
                    queue_name = q.get_name();
                }
                let cq = CreateQueue {
                    action_id: 0,
                    durable: is_durable,
                    queue: queue_name.clone().to_string(),
                    nowait: false
                };
                r = broker_utils::create_queue(&ch, cq).await;
                if r.is_err(){
                    break;
                }
            }
            if r.is_err(){
                break;
            }
        }
        let _r = broker.release_channel(ch);
        sema_arc.add_permits(1);
    }
    r
}


/// Setup the broker
///
/// # Arguments
/// * `broker` - Reference to the broker
pub async fn setup_broker(broker: &mut RabbitMQBroker) -> Result<bool, SetupError>{
    let exchange_result = setup_exchanges(broker).await;
    if exchange_result.is_ok(){
        let queue_result = setup_queues(broker).await;
        if queue_result.is_ok() {
            let queue_bind_result = bind_queues(broker).await;
            if queue_bind_result.is_ok(){
                Ok(true)
            }else{
                Err(SetupError)
            }
        }else{
            Err(SetupError)
        }
    }else{
        Err(SetupError)
    }
}


#[cfg(test)]
mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::sync::Arc;

    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;
    use tokio::sync::RwLock;

    use crate::backend::config::BackendConfig;
    use crate::broker::amqp::rabbitmq_async;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::message_structure::amqp::exchange::Exchange;
    use crate::message_structure::amqp::queue::AMQPQueue;
    use crate::message_structure::queues::GenericQueue;
    use crate::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use crate::replication::replication::HAPolicy;
    use crate::router::router::{Router, Routers};

    use super::*;

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 10;
        cannon_conf
    }

    fn get_runtime() -> Arc<Runtime>{
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(4).enable_all().build();
        Arc::new(rtr.unwrap())
    }

    fn get_routers() -> Routers{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf =  AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let q= AMQPQueue::new("lapin_test_queue_two".to_string(), Some("test_exchange".to_string()), Some("test_route".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), true, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(),queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    #[test]
    pub fn should_create_a_consumer(){
        let rt = get_runtime();
        let cfg = get_config();
        rt.clone().block_on(async move {
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let rbroker_result = RabbitMQBroker::new(
                cfg, get_routers(), result_store, rt).await;
            assert!(rbroker_result.is_ok());
            let mut rbroker = rbroker_result.ok().unwrap();
            let _sr = rbroker.setup().await;
            let _sr = rabbitmq_async::setup_queues(&mut rbroker).await;
            let r = rabbitmq_async::get_consumer(&mut rbroker, "lapin_test_queue_two".to_string(), "test_consumer".to_string()).await;
            let _conn = r.ok().unwrap();
            rbroker.close().await;
        });
    }

    #[test]
    pub fn should_setup_queues_async(){
        let rt = get_runtime();
        let cfg = get_config();
        let _broker = rt.clone().block_on(async move{
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let mut rbroker = RabbitMQBroker::new(
                cfg.clone(), get_routers(), result_store, rt).await.unwrap();
            let _r = rabbitmq_async::setup_queues(&mut rbroker).await;
        });
    }

}
