//! Implementation of available brokers in a non-asynchronous manner.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::AtomicU8;

use amq_protocol_types::FieldTable;
use lapin::{Channel, Consumer};
use lapin::options::*;
use tokio::sync::{Mutex as TokioMutex, RwLock, Semaphore};
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;
use uuid::Uuid;

use crate::backend::types::AvailableBackend;
use crate::broker::amqp::broker_utils;
use crate::broker::message::configs::exchange_bind::ExchangeBind;
use crate::broker::message::configs::exchange_create::ExchangeCreate;
use crate::broker::message::configs::queue_bind::QueueBind;
use crate::broker::message::configs::queue_declare::CreateQueue;
use crate::broker::message::configs::queue_drop::DropQueue;
use crate::broker::message::configs::queue_purge::PurgeQueue;
use crate::broker::message::configs::task::Task;
use crate::config::config::CannonConfig;
use crate::connection::amqp::channel_pool::RabbitMQChannelPool;
use crate::connection::amqp::rabbit_mq_connection_pool::RabbitMQConnectionPool;
use crate::error::exchange_error::ExchangeError;
use crate::error::pool_creation_error::PoolCreationError;
use crate::error::publish_error::PublishError;
use crate::error::queue_error::QueueError;
use crate::error::send_error::QueueSendError;
use crate::error::setup_error::SetupError;
use crate::message_protocol::stream::StreamConfig;
use crate::message_structure::amqp::exchange::Exchange;
use crate::message_structure::amqp::queue::AMQPQueue;
use crate::message_structure::queue_trait::QueueHandler;
use crate::message_structure::queues::GenericQueue;
use crate::result::asynchronous::AsyncResult;
use crate::router::router:: Routers;
use crate::task::config::TaskConfig;
use crate::task::result::TaskResponse;
use tokio::runtime::Runtime;

/// RabbitMQ Broker
///
/// # Arguments
/// * `channel_pool` - Pool of available channels created from the connection pool
/// * `channel_pool_semaphore` - Semaphore for controlling access to channels
/// * `num_failures` - Maximum number of allowed failures
/// * `calls_per_failure` - Maximum number of allowed failures occurs within this number of calls
/// * `connection_pool` - Connection pool for establishing channels
/// * `routers` - Routers object containing organized queues, exchanges, and routing keys
#[allow(dead_code)]
pub struct RabbitMQBroker{
    channel_pool: Arc<TokioMutex<RabbitMQChannelPool>>,
    channel_pool_semaphore: Arc<Semaphore>,
    num_failures: AtomicU8,
    connection_pool: RabbitMQConnectionPool,
    routers: Routers,
    result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>
}


/// Broker implementation
impl RabbitMQBroker{

    /// Setup the broker.
    async fn setup_broker(&mut self) -> Result<bool, SetupError>{
        let exchange_result= self.setup_exchanges().await;
        if exchange_result.is_ok(){
            let queue_result = self.setup_queues().await;
            if queue_result.is_ok() {
                let queue_bind_result = self.bind_queues().await;
                if queue_bind_result.is_ok(){
                    Ok(true)
                }else{
                    Err(SetupError)
                }
            }else{
                Err(SetupError)
            }
        }else{
            Err(SetupError)
        }
    }

    /// Perform setup including any ancillary broker tasks
    pub async fn setup(&mut self) -> Result<bool, SetupError>{
        self.setup_broker().await
    }

    /// Get the routers for the broker
    pub fn broker_routers(&mut self) -> &Routers{
        &self.routers
    }

    /// Close the broker asynchronously
    pub async fn close(&mut self){
        self.connection_pool.close().await;
    }

    /// Send a task but do not create an async result
    ///
    /// # Arguments
    /// * `config` - configuration
    /// * `task` - The task config
    /// * `stream_config` - The optional message body
    /// * `root_id` - The root id
    /// * `backend_type` - Backend type
    pub async fn send(
        &mut self,
        config: CannonConfig<'static>,
        task: TaskConfig,
        stream_config: Option<StreamConfig>) -> Result<JoinHandle<Result<bool, PublishError>>, ()>{
        let ch_arc = self.channel_pool.clone();
        let sema = self.get_channel_semaphore().clone();
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let exchange = task.get_exchange().clone();
        let stream = stream_config.clone();
        let chopt = self.get_channel().await;
        if chopt.is_some() {
            let ch = chopt.unwrap();
            let accept_content = config.accept_content.clone();
            let enc_type = config.encoding_type.clone();
            let rkey = task.get_routing_key().clone().unwrap();
            let exc = exchange.unwrap();
            let handle = tokio::spawn(async move {
                let task_msg = Task::new(
                    task,
                    exc.as_str(),
                    rkey.as_str(),
                    stream,
                    None,
                    None);
                let r = broker_utils::handle_task(
                    ch_arc,
                    ch,
                    task_msg,
                    accept_content.to_string(),
                    enc_type.to_string()).await;
                sema.add_permits(1);
                r
            });
            Ok(handle)
        }else {
            Err(())
        }
    }

    /// Send a task to a future and return the async result.
    ///
    /// # Arguments
    /// * `config` - configuration
    /// * `task` - The task config
    /// * `stream_config` - The optional message body
    /// * `root_id` - The root id
    /// * `backend_type` - Backend type
    pub async fn send_task(
        &mut self,
        config: CannonConfig<'static>,
        task: TaskConfig,
        stream_config: Option<StreamConfig>,
        root_id: Option<String>,
        backend_type: AvailableBackend)  -> Result<AsyncResult, QueueSendError>{
        let task_name = task.get_parent_id();
        let arc_store = self.result_store.clone();
        let reply_task = task.clone();
        let arc_task = task.clone();
        let arc_stream = stream_config.clone();
        let rid_opt = root_id.clone();
        let (sender, receiver) = tokio::sync::mpsc::channel(1);
        let msender = Arc::new(TokioMutex::new(sender));
        let rec_arc = Arc::new(TokioMutex::new(receiver));
        let mut rid = format!("{}", Uuid::new_v4());
        if rid_opt.is_some(){
            rid = rid_opt.unwrap();
        }
        let h = self.send(
            config, task.clone(), stream_config.clone()).await;
        if h.is_ok() {
            {
                let store = arc_store.clone();
                let mut wlock = store.write().await;
                (*wlock).insert(task_name, msender);
                drop(wlock);
            }
            let mut result = AsyncResult::new(
                arc_task.clone(),
                arc_stream,
                arc_task.get_time_limit() as usize,
                rid,
                backend_type,
                rec_arc,
                arc_store
            ).await;
            let handle = h.unwrap();
            result.set_send_handle(handle);
            result.reply_to.replace(reply_task.get_reply_to().to_string());
            Ok(result)
        }else{
            Err(QueueSendError)
        }
    }

    /// Get the channel semaphore
    pub fn get_channel_semaphore(&self) -> Arc<Semaphore>{
        self.channel_pool_semaphore.clone()
    }

    /// Obtain a channel from the unerlying pool
   pub async fn get_channel(&self) -> Option<Channel>{
        let mut chlock = self.channel_pool.lock().await;
        chlock.get_channel()
   }

    /// Release a channel back to the pool
    ///
    /// # Arguments
    /// * `ch` - The channel to release
    pub async fn release_channel(&self, ch: Channel){
        let mut mgaurd = self.channel_pool.lock().await;
        mgaurd.release(ch);
    }

    /// Get the routers
    pub fn get_routers(&self) -> Routers{
        self.routers.clone()
    }

    /// Creates a consumer for a rabbitmq broker. This should be setup for each specific queue to
    /// consume from.
    ///
    /// # Arguments
    /// * `queue` - The queue to consume from
    /// * `consumer_tag` - tag to use in the consumer
    pub async fn get_consumer(&mut self, queue: String, consumer_tag: String) -> Result<Consumer, ()>{
        let channel_arc = self.channel_pool.clone();
        let sema = self.channel_pool_semaphore.clone();
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some(){
            let ch = ch_opt.unwrap();
            let consumer_opt = ch.basic_consume(
                &queue, &consumer_tag, BasicConsumeOptions::default(), FieldTable::default()).await;
            if consumer_opt.is_ok(){
                {
                    let mut charc = channel_arc.lock().await;
                    charc.release(ch);
                }
                sema.add_permits(1);
                Ok(consumer_opt.unwrap())
            }else{
                {
                    let mut charc = channel_arc.lock().await;
                    charc.release(ch);
                }
                sema.add_permits(1);
                Err(())
            }
        }else{
            Err(())
        }
    }

    /// Purge a specific queue
    ///
    /// # Arguments
    /// * `queue` - The queue to purge from
    pub async fn purge_queue(&mut self, queue: AMQPQueue) -> Result<bool, QueueError>{
        let channel_arc = self.channel_pool.clone();
        let sema = self.channel_pool_semaphore.clone();
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            let pq = PurgeQueue {
                action_id: 0,
                queue: queue.get_name().to_string(),
                nowait: false
            };
            let r = broker_utils::purge_queue(&ch, pq).await;
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
            sema.add_permits(1);
            r
        }else{
            sema.add_permits(1);
            Err(QueueError)
        }
    }

    /// Handles the queue purge.
    ///
    /// # Arguments
    /// * `queue` - AMQPQueue to purge
    /// * `sema` - Semaphore controlling access to the channel pool
    /// * `channel_arc` - Arc wrapping hte channel pool semaphore
    async fn handle_queue_purge(
        queue: AMQPQueue,
        sema: Arc<Semaphore>,
        channel_arc: Arc<TokioMutex<RabbitMQChannelPool>>) -> Result<bool, QueueError>{
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = channel_arc.lock().await.get_channel();
        if ch_opt.is_some(){
            let ch = ch_opt.unwrap();
            let pq = PurgeQueue{
                action_id: 0,
                queue: queue.get_name().to_string(),
                nowait: false
            };
            let r = broker_utils::purge_queue(&ch, pq).await;
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
            sema.add_permits(1);
            r
        }else{
            sema.add_permits(1);
            Err(QueueError)
        }
    }

    /// Obtain the queue purge handles. Schedules the purge operation as well.
    ///
    /// # Arguments
    /// * `queue_map` - Flattened map of queues to purge
    async fn get_purge_queue_handles(
        &mut self,
        queue_map: HashMap<String, AMQPQueue>) -> Vec<JoinHandle<Result<bool, QueueError>>>{
        let mut handles = vec![];
        for key in queue_map.keys(){
            let queue_opt = queue_map.get(key);
            if queue_opt.is_some(){
                let queue = queue_opt.unwrap().clone();
                let sema = self.channel_pool_semaphore.clone();
                let channel_arc = self.channel_pool.clone();
                let h = tokio::spawn(async move{
                    RabbitMQBroker::handle_queue_purge(queue, sema, channel_arc).await
                });
                handles.push(h);
            }
        }
        handles
    }

    /// Purge data from all queues. Blocks until complete
    pub async fn purge_queues(&mut self) -> Result<bool, QueueError>{
        let queue_map = self.get_queue_map();
        let handles = self.get_purge_queue_handles(queue_map).await;
        let mut all_ok = true;
        for handle in handles{
            let r = handle.await;
            if r.is_err(){
                all_ok = false;
            }
        }
        if all_ok{
            Ok(true)
        }else{
            Err(QueueError)
        }
    }

    /// Handles the queue drop.
    ///
    /// # Arguments
    /// * `queue` - AMQPQueue to purge
    /// * `sema` - Semaphore controlling access to the channel pool
    /// * `channel_arc` - Arc wrapping hte channel pool semaphore
    async fn handle_drop_queue(queue: AMQPQueue, sema: Arc<Semaphore>, channel_arc: Arc<TokioMutex<RabbitMQChannelPool>>) -> Result<bool, QueueError>{
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            let dq = DropQueue{
                action_id: 0,
                queue: queue.get_name().to_string(),
                nowait: false
            };
            let r = broker_utils::drop_queue(&ch, dq).await;
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
            sema.add_permits(1);
            r
        }else{
            sema.add_permits(1);
            Err(QueueError)
        }
    }

    /// Drop the related queue. Blocks until finished. Schedules the drop function.
    ///
    /// # Arguments
    /// * `queue` - AMQPQueue to drop
    pub async fn drop_queue(&mut self, queue: AMQPQueue) -> Result<bool, QueueError>{
        let sema = self.channel_pool_semaphore.clone();
        let channel_arc = self.channel_pool.clone();
        RabbitMQBroker::handle_drop_queue(queue, sema, channel_arc).await
    }

    /// Get a flatter queue map for use in dropping queues.
    fn get_queue_map(&mut self) -> HashMap<String, AMQPQueue>{
        let routers = self.routers.match_routers(".*");
        let mut queue_map = HashMap::<String, AMQPQueue>::new();
        for router_opt in routers {
            if router_opt.is_some() {
                let router = router_opt.unwrap();
                let queues = router.get_queues();
                for queue_ref in queues {
                    let queue = queue_ref.clone();
                    if let GenericQueue::AMQPQueue(queue) = queue {
                        if queue_map.contains_key(queue.get_name()) == false{
                            queue_map.insert(queue.get_name().to_string(), queue);
                        }
                    }
                }
            }
        }
        queue_map
    }

    /// Get the join handles corresponding to a drop action for each entry in the queue map. Creates
    /// and schedules the drop actions as well
    ///
    /// # Arguments
    /// * `queue_map` - Flattened map of queues to drop
    async fn get_drop_queue_handles(
        &mut self,
        queue_map: HashMap<String, AMQPQueue>) -> Vec<JoinHandle<Result<bool, QueueError>>>{
        let mut handles: Vec<JoinHandle<Result<bool, QueueError>>> = vec![];
        for key in queue_map.keys(){
            let queue_opt = queue_map.get(key);
            if queue_opt.is_some(){
                let queue = queue_opt.unwrap().clone();
                let sema = self.channel_pool_semaphore.clone();
                let channel_arc = self.channel_pool.clone();
                let h = tokio::spawn(async move{
                    let r = RabbitMQBroker::handle_drop_queue(queue, sema, channel_arc).await;
                    r
                });
                handles.push(h);
            }
        }
        handles
    }

    ///Drop all queues from the routers. Does not change the default routers
    pub async fn drop_queues(&mut self) -> Result<bool, QueueError>{
        let queue_map = self.get_queue_map();
        let handles = self.get_drop_queue_handles(queue_map).await;
        let mut all_ok = true;
        for handle in handles{
            let r = handle.await;
            if r.is_err(){
                all_ok = false;
            }
        }
        if all_ok{
            Ok(true)
        }else{
            Err(QueueError)
        }
    }

    /// Bind a queue to an exchange
    ///
    /// # Arguments
    /// * `routing_key` - Routing key
    /// * `exc` - Exchange object
    /// * `queue` - The queue name
    #[allow(dead_code)]
    async fn bind_queue(&mut self, routing_key: String, exc: Exchange, queue: String) -> Result<bool, ExchangeError>{
        let sema = self.channel_pool_semaphore.clone();
        let channel_arc = self.channel_pool.clone();
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            let queue_bind = QueueBind {
                action_id: 0,
                exchange: exc.get_name().to_string(),
                routing_key: routing_key,
                nowait: false,
                queue: queue
            };
            let _r = broker_utils::bind_queue(&ch, queue_bind).await;
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
        }
        sema.add_permits(1);
        Ok(true)
    }

    /// Asynchronously handle the queue bind
    ///
    /// # Arguments
    /// * `exc` - The exchange
    /// * `rkey` - The routing key
    /// * `queues` - A vector of queues to bind to the routing key
    /// * `sema` - Semaphore controlling access to the channel pool
    /// * `channel_arc` - Arc wrapping the channel pool
    async fn handle_bind_queue(
        exc: Exchange,
        rkey: String,
        queues: Vec<GenericQueue>,
        sema: Arc<Semaphore>,
        channel_arc: Arc<TokioMutex<RabbitMQChannelPool>>) -> Result<bool, ()>{
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        let mut all_ok = false;
        if ch_opt.is_some() {
            all_ok = true;
            let ch = ch_opt.unwrap();
            for j in 0..queues.len() {
                let q = queues.get(j).unwrap();
                if let GenericQueue::AMQPQueue(q) = q {
                    let queue_bind = QueueBind {
                        action_id: 0,
                        exchange: exc.get_name().to_string(),
                        routing_key: rkey.to_string(),
                        nowait: false,
                        queue: q.get_name().to_string()
                    };
                    let r = broker_utils::bind_queue(&ch, queue_bind).await;
                    if r.is_err(){
                        all_ok = false;
                    }
                }
            }
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
        }
        sema.add_permits(1);
        if all_ok{
            Ok(true)
        }else{
            Err(())
        }
    }

    /// Bind queues to the exchanges. Routing keys route to di
    async fn bind_queues(&mut self) -> Result<bool, ExchangeError>{
        let routers = self.routers.match_routers(".*");
        let mut handles = vec![];
        let mut all_ok = true;
        for i in 0..routers.len() {
            let router_opt = routers.get(i).unwrap();
            let router = router_opt.unwrap().clone();
            let exc = router.get_exchange().unwrap().clone();
            let rkey = router.get_routing_key().clone();
            let queues = router.get_queues().clone();
            let sema = self.channel_pool_semaphore.clone();
            let channel_arc = self.channel_pool.clone();
            let handle = tokio::spawn(async move {
                RabbitMQBroker::handle_bind_queue(exc, rkey, queues, sema, channel_arc).await
            });
            handles.push(handle);
        }
        for handle in handles{
            let r = handle.await;
            if r.is_err(){
                all_ok = false;
            }
        }
        if all_ok{
            Ok(true)
        }else{
            Err(ExchangeError)
        }
    }

    /// Bind queues and exchanges using the routers
    ///
    /// # Arguments
    /// * `bind_args` - Bind arguments
    pub async fn bind_exchange(&mut self, bind_args: ExchangeBind) -> Result<bool, ExchangeError>{
        let sema = self.channel_pool_semaphore.clone();
        let channel_arc = self.channel_pool.clone();
        let mut r: Result<bool, ExchangeError> = Err(ExchangeError);
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some(){
            let ch = ch_opt.unwrap();
            r = broker_utils::bind_exchange(&ch, bind_args).await;
        }
        sema.add_permits(1);
        r
    }

    /// Creates the exchange.
    ///
    /// # Arguments
    /// * `sema` - Semaphore controlling access to the channel pool
    /// * `channel_arc` - Arc containing the channel pool
    /// * `routers` - Routers to match on
    async fn handle_exchange_setup(
        sema: Arc<Semaphore>, channel_arc: Arc<TokioMutex<RabbitMQChannelPool>>, routers: Routers) -> Result<bool, ExchangeError>{
        let permit = sema.acquire().await.unwrap();
        permit.forget();
        let mut is_completed: bool = true;
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            let exchange_names = broker_utils::get_exchange_names(
                routers);
            for (name, exchange_type) in &exchange_names {
                let create_args = ExchangeCreate {
                    action_id: 0,
                    durable: true,
                    exchange: name.to_string(),
                    exchange_type: exchange_type.clone(),
                    nowait: false
                };
                let _r = broker_utils::create_exchange(&ch, create_args).await;
            };
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
        }else{
            is_completed = false;
        }
        sema.add_permits(1);
        if is_completed {
            Ok(true)
        } else {
            Err(ExchangeError)
        }
    }

    /// Setup the exchanges
    async fn setup_exchanges(&mut self) -> Result<bool, ExchangeError>{
        let sema = self.channel_pool_semaphore.clone();
        let channel_arc = self.channel_pool.clone();
        let routers = self.routers.clone();
        RabbitMQBroker::handle_exchange_setup(sema, channel_arc, routers).await
    }

    /// Handle queue setup asynchronously
    ///
    /// # Arguments
    /// * `queues` - Queues from the router to setup
    /// * `sema` - Channel semaphore controlling access to the channel pool
    /// * `channel_arc` - Arc wrapping the channel pool
    async fn handle_queue_setup(
        queues: Vec<GenericQueue>, sema: Arc<Semaphore>, channel_arc: Arc<TokioMutex<RabbitMQChannelPool>>){
        let ch_opt = {
            let mut charc = channel_arc.lock().await;
            charc.get_channel()
        };
        if ch_opt.is_some() {
            let ch = ch_opt.unwrap();
            let permit = sema.acquire().await.unwrap();
            permit.forget();
            for j in 0..queues.len() {
                let q = queues.get(j).unwrap();
                let mut is_durable = false;
                let mut queue_name = "";
                if let GenericQueue::AMQPQueue(q) = q {
                    is_durable = q.is_durable();
                    queue_name = q.get_name();
                }
                let cq = CreateQueue {
                    action_id: 0,
                    durable: is_durable,
                    queue: queue_name.clone().to_string(),
                    nowait: false
                };
                let _r = broker_utils::create_queue(&ch,cq).await;
            }
            {
                let mut charc = channel_arc.lock().await;
                charc.release(ch);
            }
        }
        sema.add_permits(1);
    }

    /// Setup the broker. Creates queues and exchanges as required. Then creates the futures
    async fn setup_queues(&mut self) -> Result<bool, QueueError>{
        let routers = self.routers.match_routers(".*");
        let mut handles = Vec::<JoinHandle<()>>::new();
        let mut all_ok = true;
        for i in 0..routers.len() {
            let router_opt = routers.get(i).unwrap();
            let router = router_opt.unwrap().clone();
            let queues = router.get_queues().clone();
            let sema = self.channel_pool_semaphore.clone();
            let channel_arc = self.channel_pool.clone();
            let handle = tokio::spawn(async move {
                RabbitMQBroker::handle_queue_setup(queues, sema, channel_arc).await;
            });
            handles.push(handle);
        }
        for handle in handles{
            let r = handle.await;
            if r.is_err(){
                all_ok = false;
            }
        }
        if all_ok{
            Ok(true)
        }else{
            Err(QueueError)
        }
    }

    /// Adds a channel back to the pool. This should be called when creating a consumer to avoid
    /// depleting the channel pool.
    pub async fn add_channel_to_pool(&mut self){
        let mut chlock = self.channel_pool.lock().await;
        chlock.add_channels(1, &mut self.connection_pool).await;
        self.channel_pool_semaphore.add_permits(1);
    }

    /// Asynchronously create a broker
    ///
    /// # Arguments
    /// * `config` - Application configuration
    /// * `routers` - Routers object
    pub async fn new(
        config: CannonConfig<'static>,
        routers: Routers,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) -> Result<RabbitMQBroker, PoolCreationError>{
        let conn_pool_res = RabbitMQConnectionPool::create(
            config.clone(), true).await;
        if conn_pool_res.is_ok() {
            let mut conn_pool = conn_pool_res.ok().unwrap();
            let num_permits = config.num_broker_channels as usize;
            let sema = Semaphore::new(num_permits);
            let ch_pool = RabbitMQChannelPool::create(num_permits, &mut conn_pool).await;
            let rmq = RabbitMQBroker {
                channel_pool: Arc::new(TokioMutex::new(ch_pool)),
                channel_pool_semaphore: Arc::new(sema),
                num_failures: AtomicU8::new(0),
                connection_pool: conn_pool,
                routers,
                result_store
            };
            Ok(rmq)
        }else{
            Err(PoolCreationError)
        }
    }
}


#[cfg(test)]
mod tests{
    use std::collections::HashSet;
    use std::env;
    use std::sync::atomic::AtomicBool;

    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;
    use tokio::sync::RwLock;

    use crate::backend::config::BackendConfig;
    use crate::backend::redis::backend::RedisResultHandler;
    use crate::backend::types::AvailableBackend;
    use crate::broker::broker_type;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::message_structure::amqp::exchange::Exchange;
    use crate::message_structure::amqp::queue::AMQPQueue;
    use crate::message_structure::queues::GenericQueue;
    use crate::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use crate::replication::replication::HAPolicy;
    use crate::router::router::{Router, Routers};

    use super::*;

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 20;
        cannon_conf.num_broker_connections = 5;
        cannon_conf
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(10)
            .build().unwrap();
        Arc::new(rt)
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_routers() -> Routers{
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let q= AMQPQueue::new(
            "lapin_test_queue".to_string(), Some("test_exchange".to_string()), Some("test_key".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), false, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(),queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    #[test]
    fn should_create_broker(){
        let rt = get_runtime();
        let config = get_config();
        let rts = get_routers();
        rt.clone().block_on(async move {
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker = RabbitMQBroker::new(
                config,rts, result_store, rt.clone()).await;
            assert!(broker.is_ok());
            let r = broker.ok().unwrap().setup().await;
            assert!(r.is_ok());
        });
    }

    async fn perform_setup(broker: &mut RabbitMQBroker){
        let exch_result = broker.setup_exchanges().await;
        assert!(exch_result.is_ok());
        let queue_result = broker.setup_queues().await;
        assert!(queue_result.is_ok());
        let qbind_result = broker.bind_queues().await;
        assert!(qbind_result.is_ok());
    }

    #[test]
    fn should_create_exchanges(){
        let runtime = get_runtime();
        let config = get_config();
        runtime.clone().block_on(async move {
            let rts = get_routers();
            let r = rts.match_routers(".*");
            assert!(r.len() > 0);
            let vo = r.get(0).unwrap().unwrap().get_exchange();
            assert!(vo.is_some());
            assert!(vo.unwrap().get_name().eq("test_exchange"));
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                config, rts, result_store, runtime).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            let exch_result = broker.setup_exchanges().await;
            assert!(exch_result.is_ok());
            broker.close().await;
        });
    }

    #[test]
    fn should_create_router_queues_and_exchanges(){
        let rt = get_runtime();
        let cfg = get_config();
        rt.clone().block_on(async move {
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, rt).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
        });
    }

    #[test]
    fn should_drop_queues_when_requested(){
        let rt = get_runtime();
        let cfg = get_config();
        let new_arc = rt.clone();
        let h = rt.clone().spawn(async move {
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, new_arc).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            let r = broker.drop_queues().await;
            assert!(r.is_ok());
        });
        rt.clone().block_on(async move{
           let _r = h.await;
        });
    }

    #[test]
    fn should_purge_queue_on_request(){
        let rt = get_runtime();
        let cfg = get_config();
        rt.clone().block_on(async move {
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, rt).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            let r = broker.purge_queues().await;
            assert!(r.is_ok());
        });
    }

    fn get_task_config() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg.clone(),
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("test_1".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    #[test]
    fn should_asynchronously_send_tasks(){
        let runtime = get_runtime();
        let cfg = get_config();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        let rts = get_routers();
        let _fr = runtime.clone().block_on(async move{
            let broker_result  = RabbitMQBroker::new(
                cfg.clone(),rts, result_store, runtime).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            let config = get_task_config();
            let sc = StreamConfig::new(None, None);
            let bck_cfg = get_backend_config();
            let backend_type = AvailableBackend::Redis((bck_cfg, 1));
            let r= broker.send_task(
                cfg, config,Some(sc),  None, backend_type).await;
            assert!(r.is_ok());
            let b = r.unwrap();
            let _r = b.send_handle.unwrap().await;
            broker.close().await;
        });
    }

    #[test]
    fn should_send_tasks(){
        let runtime = get_runtime();
        let cfg = get_config();
        let new_arc = runtime.clone();
        let h = runtime.spawn(async move {
            let config = get_task_config();
            let sc = StreamConfig::new(None, None);
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg.clone(), rts, result_store, new_arc).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            let bck_cfg = get_backend_config();
            let backend_type = AvailableBackend::Redis((bck_cfg, 1));
            let r = broker.send_task(
                cfg, config, Some(sc), None, backend_type).await;
            assert!(r.is_ok());
            let fr = r.ok().unwrap().send_handle.unwrap().await;
            assert!(fr.is_ok());
            broker.close().await;
        });
        runtime.block_on(async move{
           let _r = h.await;
        });
    }

    #[test]
    fn should_run_at_load(){
        let rt = get_runtime();
        let cfg  = get_config();
        let new_arc = rt.clone();
        let handle = rt.clone().spawn(async move {
            let app_status = Arc::new(AtomicBool::new(true));
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg.clone(), rts, result_store, new_arc).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            let mut load_vec = Vec::<AsyncResult>::new();
            let bck_cfg = get_backend_config();
            let mut ids = HashSet::<String>::new();
            ids.insert("test_1".to_string());
            let store = Arc::new(
                RwLock::new(HashMap::new()));
            let arc_bck_cfg = bck_cfg.clone();
            let handler = RedisResultHandler::new(
                cfg.clone(),
                app_status.clone(),
                arc_bck_cfg,
                2,
                Some(ids),
                store.clone()).await;
            for i in 0..(100000 as i32) {
                let config = get_task_config();
                let btype = AvailableBackend::Redis((bck_cfg.clone(), 1));
                let sc = StreamConfig::new(
                    None, None);
                let r = broker.send_task(
                    cfg.clone(),
                    config.clone(),
                    Some(sc),
                    None,
                    btype).await;
                let ar = r.ok().unwrap();
                load_vec.push(ar);
                if load_vec.len() > 500 || i >= 9999 {
                    for mut ar in load_vec {
                        let r = ar.await_send().await;
                        assert!(r.is_ok());
                    }
                    {
                        let ast = store.clone();
                        let mut st = ast.write().await;
                        (*st).clear();
                    }
                    load_vec = vec![];
                }
            }
            handler.close().await;
            broker.close().await;
        });
        rt.block_on(async move{
            let _r = handle.await;
        })
    }

    #[test]
    fn should_close(){
        let rt = get_runtime();
        let cfg = get_config();
        let rts = get_routers();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        rt.clone().block_on(async move {
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, rt).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            perform_setup(&mut broker).await;
            broker.close().await;
        });
    }

    #[test]
    fn should_get_consumer(){
        let rt = get_runtime();
        let cfg = get_config();
        let rts = get_routers();
        rt.clone().block_on(async move {
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, rt).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            let qname = "test_queue".to_string();
            let ctag = "consumer_tag".to_string();
            let _r = broker.get_consumer(qname, ctag).await;
            broker.close().await;
        });
    }

    #[test]
    fn should_add_channel_to_existing_pool(){
        let rt = get_runtime();
        let cfg = get_config();
        rt.clone().block_on(async move {
            let rts = get_routers();
            let result_store = Arc::new(RwLock::new(HashMap::new()));
            let broker_result = RabbitMQBroker::new(
                cfg, rts, result_store, rt).await;
            assert!(broker_result.is_ok());
            let mut broker = broker_result.ok().unwrap();
            let charc = broker.channel_pool.clone();
            let mut chsz = charc.lock().await;
            let chsize_ognl = chsz.get_current_num_channels();
            drop(chsz);
            broker.add_channel_to_pool().await;
            let charc2 = broker.channel_pool.clone();
            let mut chsz2 = charc2.lock().await;
            let chsize = chsz2.get_current_num_channels();
            assert!(chsize_ognl < chsize);
            broker.close().await;
        });
    }
}
