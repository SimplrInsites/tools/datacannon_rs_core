//! Broker Utilities for RabbitMq
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::sync::Arc;

use amq_protocol_types::FieldTable;
use lapin::{Channel, ExchangeKind};
use lapin::options::*;
use tokio::sync::Mutex;

use crate::broker::message::configs::exchange_bind::ExchangeBind;
use crate::broker::message::configs::exchange_create::ExchangeCreate;
use crate::broker::message::configs::exchange_drop::DropExchange;
use crate::broker::message::configs::prefetch_set::SetPrefetchLimit;
use crate::broker::message::configs::queue_bind::QueueBind;
use crate::broker::message::configs::queue_declare::CreateQueue;
use crate::broker::message::configs::queue_drop::DropQueue;
use crate::broker::message::configs::queue_purge::PurgeQueue;
use crate::broker::message::configs::task::Task;
use crate::connection::amqp::channel_pool::RabbitMQChannelPool;
use crate::error::exchange_error::ExchangeError;
use crate::error::publish_error::PublishError;
use crate::error::qos_error::QOSError;
use crate::error::queue_error::QueueError;
use crate::router::router::Routers;

/// Get the exchange names from the provided routers
///
/// # Arguments
/// * `routers` - Object containing the routers
pub fn get_exchange_names(routers: Routers) -> HashMap<String, ExchangeKind>{
    let router_matches = routers.match_routers(".*");
    let mut exchange_names = HashMap::<String, ExchangeKind>::new();
    let mut i = 0;
    while i < router_matches.len() {
        let router_opt = router_matches.get(i).unwrap();
        let router = router_opt.unwrap();
        let exchange_opt = router.get_exchange().clone();
        if exchange_opt.is_some() {
            let exchange = exchange_opt.unwrap();
            let exchange_name = exchange.get_name().to_string();
            let r = exchange_names.get(exchange_name.as_str());
            if r.is_none() {
                exchange_names.insert(exchange_name, exchange.get_exchange_type().clone());
            }
        }
        i += 1;
    }
    exchange_names
}


/// Bind to an exchange returning a status or ExchangeError
///
/// # Arguments
/// * `channel` - A lapin channel to bind on
/// * `event` - The ExchangeBind event
pub(in crate::broker::amqp) async fn bind_exchange(channel: &Channel, event: ExchangeBind) -> Result<bool, ExchangeError>{
    let ftable = FieldTable::default();
    let mut options = ExchangeBindOptions::default();
    options.nowait = event.nowait;
    let confirm = channel.exchange_bind(
        event.exchange.as_str(),
        event.source_exchange.as_str(),
        event.routing_key.as_str(),
        options,
        ftable).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(ExchangeError)
    }
}


/// Create the exchange returning a status or ExchangeError
///
///# Arguments
/// * `channel` - The lapin channel to create on
/// * `event` - The Exchange create event
pub(in crate::broker::amqp) async fn create_exchange(channel: &Channel, event: ExchangeCreate) -> Result<bool, ExchangeError>{
    let ftable = FieldTable::default();
    let mut options = ExchangeDeclareOptions::default();
    options.durable = event.durable;
    options.nowait = event.nowait;
    let confirm = channel.exchange_declare(event.exchange.as_str(), event.exchange_type, options, ftable).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(ExchangeError)
    }
}


/// Drop an exchange returning a status or ExchangeError
///
/// # Arguments
/// * `channel` - The channel to drop on
/// * `event` - Drop Exchange arguments
#[allow(dead_code)]
pub(in crate::broker::amqp) async fn drop_exchange(channel: &Channel, event: DropExchange) -> Result<bool, ExchangeError>{
    let mut options = ExchangeDeleteOptions::default();
    options.nowait = event.nowait;
    let confirm = channel.exchange_delete(event.exchange.as_str(), options).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(ExchangeError)
    }
}


/// Create a queue. Return a status or QueueError
///
/// # Arguments
/// * `channel` - The lapin channel to create on
/// * `event` - Create queue arguments
pub(in crate::broker::amqp) async fn create_queue(channel: &Channel, event: CreateQueue) -> Result<bool, QueueError>{
    let ftable = FieldTable::default();
    let mut options = QueueDeclareOptions::default();
    options.durable = event.durable;
    options.nowait = event.nowait;
    let confirm = channel.queue_declare(event.queue.as_str(), options, ftable).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(QueueError)
    }
}


/// Purge the queue
///
/// # Arguments
/// * `channel` - The lapin channel
/// * `event` - Purge queue arguments
pub(in crate::broker::amqp) async fn purge_queue(channel: &Channel, event: PurgeQueue) -> Result<bool, QueueError>{
    let mut options = QueuePurgeOptions::default();
    options.nowait = event.nowait;
    let confirm = channel.queue_purge(event.queue.as_str(), options).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(QueueError)
    }
}


/// Bind a queue to an exchange
///F
/// # Arguments
/// * `channel` - The lapin channel
/// * `event`- Exchange bind arguments
pub(in crate::broker::amqp) async fn bind_queue(channel: &Channel, event: QueueBind) -> Result<bool, ExchangeError>{
    let ftable = FieldTable::default();
    let mut options = QueueBindOptions::default();
    options.nowait = event.nowait;
    let confirm = channel.queue_bind(
        event.queue.as_str(),
        event.exchange.as_str(),
        event.routing_key.as_str(),
        options,
        ftable).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(ExchangeError)
    }

}


/// Drop the queue
///
/// # Arguments
/// * `channel` - The lapin channel to drop on
/// * `event` - Queue drop arguments
pub(in crate::broker::amqp) async fn drop_queue(channel: &Channel, event: DropQueue) -> Result<bool, QueueError>{
    let options = QueueDeleteOptions::default();
    let confirm = channel.queue_delete(event.queue.as_str(), options).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(QueueError)
    }
}


/// Set the prefetch limit
///
/// # Arguments
/// * `channel` - The lapin channel to set the limit on
/// * `event` - Set prefetch limit argument
#[allow(dead_code)]
pub(in crate::broker::amqp) async fn set_prefetch_limit(channel: &Channel, event: SetPrefetchLimit) -> Result<bool, QOSError>{
    let mut options = BasicQosOptions::default();
    options.global = event.global;
    let confirm = channel.basic_qos(event.limit, options).await;
    if confirm.is_ok(){
        Ok(true)
    }else{
        Err(QOSError)
    }
}


/// Handle sending to a broker
///
/// # Arguments
/// * `channel_arc` - Channel pool arc`
/// * `channel` - The lapin channel to use for events
/// * `task` - Object holding the task
/// * `accept_content` - Content type to accept
/// * `encoding_type` - String encoding type
pub(in crate::broker::amqp) async fn handle_task<'a>(
    channel_arc: Arc<Mutex<RabbitMQChannelPool>>,
    channel: Channel,
    task: Task<'a>,
    accept_content: String,
    encoding_type: String) -> Result<bool, PublishError>{
    let publish_opts = BasicPublishOptions::default();
    let exc = task.get_exchange();
    let task_cfg = task.get_task_config();
    let rkey = task.get_routing_key();
    let rid = task.get_root_id().unwrap_or(format!("{}", uuid::Uuid::new_v4()));
    let msg = task_cfg.to_amqp_message(accept_content.as_str(), encoding_type.as_str()
                                       , task.get_stream(), Some(rid));
    let payload = msg.get_message_payload();
    let mut props = msg.properties.convert_to_amqp_properties();
    let task_headers = msg.headers.convert_to_btree_map();
    props = props.with_headers(FieldTable::from(task_headers));
    let r = channel.basic_publish(exc, rkey, publish_opts, payload, props).await;
    let mut charc = channel_arc.lock().await;
    let chr = charc.release(channel);
    assert!(chr);
    if r.is_ok(){
        Ok(true)
    }else {
        Err(PublishError)
    }
}


#[cfg(test)]
mod test{
    use std::env;
    use std::sync::Arc;

    use tokio::runtime::Runtime;
    use tokio::sync::{Mutex, Semaphore};

    use crate::backend::config::BackendConfig;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::channel_pool::RabbitMQChannelPool;
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::amqp::rabbit_mq_connection_pool::RabbitMQConnectionPool;
    use crate::connection::connection::ConnectionConfig;
    use crate::router::router::Routers;
    use crate::task::config::TaskConfig;

    use super::*;

    /// Get the connection pool
                    ///
                    /// # Arguments
                    /// * `pool` - The relevant RabbitMQ Connection Pool
    async fn get_channel_pool(pool: &mut RabbitMQConnectionPool) -> RabbitMQChannelPool{
        RabbitMQChannelPool::create(5000,pool).await
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 5000;
        cannon_conf.num_broker_connections = 50;
        cannon_conf
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .enable_all().build();
        Arc::new(rt.unwrap())

    }

    async fn setup_globals(rt: Arc<Runtime>) -> RabbitMQConnectionPool{
        let cfg = get_config();
        let pl = RabbitMQConnectionPool::new(
            &cfg, true).await;
        pl.ok().unwrap()
    }

    #[test]
    fn should_handle_load(){
        let cfg = get_config();
        let rt = get_runtime();
        let new_arc = rt.clone();
        let h = rt.spawn(async move {
            let mut pool = setup_globals(new_arc).await;
            let pool_sema = Arc::new(Semaphore::new(5000));
            let channel_pool = Arc::new(Mutex::new(get_channel_pool(&mut pool).await));
            let mut handles = vec![];
            for i in 0..100000 {
                let sema = pool_sema.clone();
                let permit = sema.acquire().await.unwrap();
                permit.forget();
                let channel_arc = channel_pool.clone();
                let msg_exchange = "test_exchange";
                let msg_rkey = "test_key";
                let accept_content = cfg.accept_content.clone().to_string();
                let encoding_type = cfg.encoding_type.clone().to_string();
                let task =TaskConfig::new(
                    cfg.clone(),
                    "RABBITMQ".to_string(),
                    "test_task".to_string(),
                    None,
                    None,
                    None,
                    Some("test_parent_id".to_string()),
                    None,
                    None,
                    None,
                    None,
                    None,
                    Some(0),
                    None,
                    None,
                    100000,
                    None,
                    None,
                    None
                );
                let taskmsg = Task::new(task, msg_exchange, msg_rkey, None, None, None);
                let chopt = {
                    let mut charc = channel_arc.lock().await;
                    charc.get_channel()
                };
                if chopt.is_some() {
                    let ch = chopt.unwrap();
                    let h = handle_task(
                        channel_arc.clone(),
                        ch,
                        taskmsg,
                        accept_content,
                        encoding_type);
                    handles.push(h);
                    if handles.len() >=5000 || (i + 1) == 100000{
                        for handle in handles{
                            let _r = handle.await;
                            sema.add_permits(1);
                        }
                        handles = vec![];
                    }
                }
            }
        });
        rt.block_on(async move{
           let _r = h.await;
        });
    }
}

