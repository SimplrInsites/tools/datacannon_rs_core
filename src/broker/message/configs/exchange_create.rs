//! Messages for creating the exchange
//!
//! ---
//! author: Andrew Evans
//! ---

use lapin::ExchangeKind;

/// Exchange Creation Config
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct ExchangeCreate{
    pub action_id: u128,
    pub durable: bool,
    pub exchange: String,
    pub exchange_type: ExchangeKind,
    pub nowait: bool,
}