//! Purge the queue
//!
//! ---
//! author: Andrew Evans
//! ---

/// Queue purge configuration
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct PurgeQueue{
    pub action_id: u128,
    pub queue: String,
    pub nowait: bool,
}
