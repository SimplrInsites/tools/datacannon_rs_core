//! Message protocol for broker tasks
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::task::config::TaskConfig;
use crate::message_protocol::stream::StreamConfig;
use uuid::Uuid;
use lapin::ExchangeKind;


/// Task structure
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct Task<'a>{
    action_id: u128,
    task: TaskConfig,
    stream: Option<StreamConfig>,
    root_id: Option<String>,
    exchange: &'a str,
    routing_key: &'a str,
    exchange_type: Option<ExchangeKind>,
}


/// Task implementation
impl <'a> Task<'a>{

    pub fn get_routing_key(&self) -> &'a str{
        self.routing_key
    }

    /// Obtain the exchange
    pub fn get_exchange(&self) -> &'a str{
        self.exchange
    }

    /// Get the task config
    pub fn get_task_config(&self) -> &TaskConfig{
        &self.task
    }

    /// Ge the message body
    pub fn get_stream(&self) -> Option<StreamConfig>{
        self.stream.clone()
    }

    /// Get the root id
    pub fn get_root_id(&self) -> Option<String>{
        self.root_id.clone()
    }

    /// Get the exchange type
    pub fn get_exchange_type(&self) -> Option<ExchangeKind>{
        self.exchange_type.clone()
    }

    /// Create a new task
    ///
    /// # Arguments
    /// * `task` - The task configuration
    /// * `exchange` - Exchange to send the task to
    /// * `routing_key` - Routing key to send the task to
    /// * `string_body` - Stream configuration (managed by chords and chain handlers)
    /// * `root_id` - Root or parent id managed with chords and chains
    /// * `exchange_type` - Type of Exchange
    pub fn new(task: TaskConfig, exchange: &'a str, routing_key: &'a str, stream_config: Option<StreamConfig>, root_id: Option<String>, exchange_type: Option<ExchangeKind>) -> Task<'a>{
        let action_id = Uuid::new_v4().as_u128();
        Task{
            action_id: action_id,
            task: task,
            exchange: exchange,
            routing_key: routing_key,
            stream: stream_config,
            root_id: root_id,
            exchange_type: exchange_type,
        }
    }
}
