//! Config to bind to an exchange
//!
//! ---
//! author: Andrew Evans
//! ---

use lapin::ExchangeKind;

/// Config for binding to an exchange
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct ExchangeBind{
    pub action_id: u128,
    pub exchange: String,
    pub routing_key: String,
    pub exchange_type: Option<ExchangeKind>,
    pub nowait: bool,
    pub source_exchange: String,
}
