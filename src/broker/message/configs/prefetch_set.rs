//! Config for setting the prefetch limit
//!
//! ---
//! author: Andrew Evans
//!

/// Config for setting the prefetch limit
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct SetPrefetchLimit{
    pub action_id: u128,
    pub limit: u16,
    pub global: bool,
}
