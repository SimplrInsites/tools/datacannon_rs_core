//! Config for dropping a queue
//!
//! ---
//! author: Andrew Evans
//! ---

/// Config for dropping a queue
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct DropQueue{
    pub action_id: u128,
    pub queue: String,
    pub nowait: bool,
}
