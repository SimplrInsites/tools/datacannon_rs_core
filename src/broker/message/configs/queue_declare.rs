//! Sent when declaring a queue
//!
//! ---
//! author: Andrew Evans
//! ---

/// Queue Creation config
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct CreateQueue{
    pub action_id: u128,
    pub durable: bool,
    pub queue: String,
    pub nowait: bool,
}
