//! Config to bind to an exchange
//!
//! ---
//! author: Andrew Evans
//! ---

/// Config for binding to an exchange
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct QueueBind{
    pub action_id: u128,
    pub exchange: String,
    pub routing_key: String,
    pub nowait: bool,
    pub queue: String
}
