//! For dropping an exchange
//!
//! ---
//! author: Andrew Evans
//! ---

/// Drop Exchange config
#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct DropExchange{
    pub action_id: u128,
    pub exchange: String,
    pub nowait: bool,
}
