//! Enumeration for storing a broker type
//!
//! ---
//! author: Andrew Evans
//! ---

use lapin::ExchangeKind;

use crate::broker::amqp::rabbitmq::RabbitMQBroker;
use crate::error::send_error::QueueSendError;
use crate::error::setup_error::SetupError;
use crate::message_protocol::stream::StreamConfig;
use crate::result::asynchronous::AsyncResult;
use crate::task::config::TaskConfig;

/// An enumeration of available brokers
///
/// # Arugments
/// * `RabbitMQ` - RabbitMQ broker
pub enum AvailableBroker{
    RabbitMQ(RabbitMQBroker),
}


/// The broker
pub trait Broker{

    /// close the broker which should call teardown
    fn close(&mut self);

    /// send a task
    fn send_task(&mut self, task: TaskConfig, stream_config: Option<StreamConfig>, routing_key: Option<String>, exchange_type: Option<ExchangeKind>, exchange: Option<String>, root_id: Option<String>) -> Result<AsyncResult, QueueSendError>;

    /// Setup broker. Create queues and exchanges, creating bindings; perform other setup tasks
    fn setup_broker(&mut self) -> Result<bool, SetupError>;

    /// Start the broker futures thus starting the broker
    fn start_broker(&mut self);
}
