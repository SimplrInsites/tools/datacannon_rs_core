//! Callback information for the application
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::task::result::TaskResponse;

/// Allows for storage and retreival of a callback
#[derive(Clone)]
pub struct Callback{
    pub f: fn(TaskResponse) -> ()
}
