//! Errback information for the application
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::task::result::TaskResponse;

/// Allows for storage and retreival of a errback
#[derive(Clone)]
pub struct Errback{
    pub f: fn(TaskResponse) -> ()
}
