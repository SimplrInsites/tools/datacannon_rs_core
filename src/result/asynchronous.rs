//! Asynchronous Result. The asynchronous result parent id is used to retreive results. Correlation
//! ids correlate requests to responses in rabbitmq which appears perfect. However, if a chain
//! executes, there is some overhead that needs to be added to ensure these ids match. Instead,
//! any framework should attempt to match the parent id with reply_to.
//!
//! Reply works the same as in RabbitMQ. In our backends, the result enters the reply to queue or
//! is set to the appropriate key, or published to the reply_to topic.
//!
//! If you manually set the correlation id for a single message, they should come out equal. This
//! burden is on the frameworks.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;

use tokio::sync::{Mutex as TokioMutex, Notify, RwLock};
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::task::JoinHandle;

use crate::backend::types::AvailableBackend;
use crate::error::publish_error::PublishError;
use crate::message_protocol::stream::StreamConfig;
use crate::result::callbacks::{callback::Callback, errback::Errback};
use crate::task::config::TaskConfig;
use crate::task::result::TaskResponse;

/// Result Types with one for responses and another for Acknowledgment when there is no backend
pub enum ResultType{
    RESPONSE(BackendResult),
    ACK
}

/// Stores a backend result
#[allow(dead_code)]
pub struct BackendResult{
    response: Option<TaskResponse>,
    success: bool
}


/// Asynchronous result containing a handle for the push
/// and a root id for obtaining a result by.
pub struct AsyncResult{
    pub notifier: Arc<Notify>,
    pub is_complete: Arc<AtomicBool>,
    pub task: TaskConfig,
    pub stream_config: Option<StreamConfig>,
    pub send_handle: Option<JoinHandle<Result<bool, PublishError>>>,
    pub result_timeout: Option<usize>,
    pub parent_id: String,
    pub reply_to: Option<String>,
    pub backend_channel: Arc<TokioMutex<Receiver<TaskResponse>>>,
    pub backend_type: AvailableBackend,
    pub callbacks: Vec<Callback>,
    pub errbacks: Vec<Errback>,
   result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>
}


/// Asynchronous result implementation
impl AsyncResult{

    /// Remove the key used to retreive the result.
    async fn remove_key(&mut self){
        let key = self.task.get_parent_id().clone();
        let mut wlock = self.result_store.write().await;
        let has_key = (*wlock).contains_key(&key);
        if has_key{
            let _r = (*wlock).remove(&key);
        }
    }

    /// Await the asynchronous result. The actual asnyc function should handle any timeouts.
    /// Returns a result status.
    pub async fn await_result(&mut self) -> Result<TaskResponse, ()>{
        let rx_gaurd = self.backend_channel.clone();
        let timeout = self.result_timeout.clone();
        let mut rx = rx_gaurd.lock().await;
        if timeout.is_some() {
            let millis = timeout.unwrap();
            let duration = Duration::from_millis(millis as u64);
            let is_timeout = tokio::time::timeout(duration, rx.recv()).await;
            if is_timeout.is_ok() {
                let response = is_timeout.unwrap();
                if response.is_some() {
                    let rval = response.unwrap();
                    let _r = self.remove_key();
                    Ok(rval)
                }else{
                    let _r = self.remove_key();
                    Err(())
                }
            } else {
                let _r = self.remove_key();
                Err(())
            }
        } else {
            let r = rx.recv().await;
            if r.is_some(){
                let response = r.unwrap();
                let _r = self.remove_key();
                Ok(response)
            }else{
                let _r = self.remove_key();
                Err(())
            }
        }
    }

    /// Execute the errbacks. Call after awaiting result. Calls functions in the order you
    /// inserted them in the list
    ///
    /// # Arguments
    /// * `response` - The task response
    pub fn execute_errbacks(&self, response: TaskResponse){
        if !self.errbacks.is_empty(){
            for errback in &self.errbacks{
                let f = errback.f;
                f(response.clone());
            }
        }
    }

    /// Execute the callbacks. Call after awaiting result. Calls functions in the order you
    /// inserted them in the list.
    ///
    /// # Arguments
    /// * `response` -  The task response.
    pub fn execute_callbacks(&self, response: TaskResponse){
        if !self.callbacks.is_empty(){
            for callback in &self.callbacks{
                let f = callback.f;
                f(response.clone());
            }
        }
    }

    /// Await the send portion of the result. Completes the send portion of the handle.
    /// Sets up the broker portion of the handle.
    pub async fn await_send(&mut self) -> Result<(), ()>{
        let handle = self.send_handle.take();
        let r = handle.unwrap().await;
        if r.is_ok(){
            Ok(())
        }else{
            Err(())
        }
    }

    /// Send a task asynchronously
    pub async fn send_async(&mut self) -> Result<(), ()>{
        let handle = self.send_handle.take();
        let r = handle.unwrap().await;
        if r.is_ok(){
            Ok(())
        }else{
            Err(())
        }
    }

    /// Complete the entire asynchronous result. This awaits for the sent task and
    /// then awaits for the result
    pub async fn complete(&mut self) -> Result<ResultType, ()>{
        let notify = self.notifier.clone();
        let arc_bool = self.is_complete.clone();
        let r = self.await_send().await;
        if r.is_ok(){
            let resp = self.await_result().await;
            if resp.is_ok() {
                let task_response = resp.ok().unwrap();
                let backend_response = BackendResult{
                    response: Some(task_response.clone()),
                    success: true
                };
                self.execute_errbacks(task_response.clone());
                self.execute_callbacks(task_response.clone());
                notify.notify_one();
                arc_bool.store(true, Ordering::Relaxed);
                let result_type = ResultType::RESPONSE(backend_response);
                Ok(result_type)
            }else{
                let backend_result = BackendResult{ response: None, success: false };
                let result_type = ResultType::RESPONSE(backend_result);
                notify.notify_one();
                arc_bool.store(true, Ordering::Relaxed);
                Ok(result_type)
            }
        }else{
            notify.notify_one();
            arc_bool.store(true, Ordering::Relaxed);
            Err(())
        }
    }

    /// Add an errback to the result.
    ///
    /// # Arguments
    /// * `errback` - The errback to add
    pub fn add_errback(&mut self, errback: Errback){
        self.errbacks.push(errback);
    }

    /// Add a callback for the result
    ///
    /// # Arguments
    /// * `callback` - The callback to add
    pub fn add_callback(&mut self, callback:  Callback){
        self.callbacks.push(callback);
    }


    /// Set the send handle
    ///
    /// # Arguments
    /// * `handle` - The send Join Handle
    pub fn set_send_handle(&mut self, handle: JoinHandle<Result<bool, PublishError>>){
        self.send_handle = Some(handle);
    }

    /// Create a new Asynchronous result with a preinstantiated backend handle and supplied
    /// send handle later. The backend handle must be created prior to sending the task
    /// since backends such as Redis require the use of pubsub to remain clean.
    ///
    /// # Arguments
    /// * `task` - TaskConfig
    /// * `stream_config` - Option containing the task stream configuration
    /// * `timeout` - Hard time limit for the operation
    /// * `root_id` - Root id for the operation
    /// * `reply_to` - Reply tofor geting the
    /// * `backend_type` - Backend type to setup on
    /// * `result_store` - Storage for results
    pub async fn new(
        task: TaskConfig,
        stream_config: Option<StreamConfig>,
        timeout: usize,
        parent_id: String,
        backend_type: AvailableBackend,
        receiver: Arc<TokioMutex<Receiver<TaskResponse>>>,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>) -> AsyncResult{
        let result = AsyncResult{
            notifier: Arc::new(Notify::new()),
            is_complete: Arc::new(AtomicBool::new(true)),
            task,
            stream_config,
            send_handle: None,
            result_timeout: Some(timeout),
            parent_id,
            reply_to: None,
            backend_channel: receiver,
            backend_type,
            callbacks: vec![],
            errbacks: vec![],
            result_store
        };
        result
    }
}


/// Implements getters for the backend result
impl BackendResult{

    /// Obtain the result part of the json
    pub fn get_response(&self) -> &Option<TaskResponse>{
        &self.response
    }

    /// Check whether the result succeeded
    pub fn is_success(&self) -> &bool{
        &self.success
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::{HashMap, HashSet};
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::AtomicBool;

    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;
    use tokio::sync::Notify;

    use crate::argparse::argtype::ArgType;
    use crate::backend::config::BackendConfig;
    use crate::backend::redis::backend::RedisResultHandler;
    use crate::backend::redis::producer;
    use crate::backend::redis::producer::RedisProducer;
    use crate::backend::types::AvailableBackend;
    use crate::broker::amqp::rabbitmq::RabbitMQBroker;
    use crate::broker::broker_type;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::error::send_error::QueueSendError;
    use crate::message_protocol::stream::StreamConfig;
    use crate::message_structure::amqp::exchange::Exchange;
    use crate::message_structure::amqp::queue::AMQPQueue;
    use crate::message_structure::queues::GenericQueue;
    use crate::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use crate::replication::replication::HAPolicy;
    use crate::result::asynchronous::{AsyncResult, ResultType};
    use crate::router::router::{Router, Routers};
    use crate::task::config::TaskConfig;
    use crate::task::result::{Response, TaskResponse};

    use super::*;

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(4)
            .enable_all()
            .build();
        Arc::new(rt.unwrap())
    }

    fn get_task_config() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_route".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("test_1".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_stream_config() -> StreamConfig{
        StreamConfig::new(None, None)
    }

    fn get_result() -> AsyncResult{
        let (_sender, receiver) = tokio::sync::mpsc::channel(1);
        let arc_receiver = Arc::new(TokioMutex::new(receiver));
        AsyncResult{
            notifier: Arc::new(Notify::new()),
            is_complete: Arc::new(Default::default()),
            task: get_task_config(),
            stream_config: Some(get_stream_config()),
            send_handle: None,
            result_timeout: None,
            parent_id: "test_parent_id".to_string(),
            reply_to: Some("test_1".to_string()),
            backend_channel: arc_receiver,
            backend_type: AvailableBackend::NONE,
            callbacks: vec![],
            errbacks: vec![],
            result_store: Arc::new(RwLock::new(HashMap::new()))
        }
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_routers() -> Routers{
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let q= AMQPQueue::new(
            "lapin_test_queue".to_string(), Some("test_exchange".to_string()), Some("test_key".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), true, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(),queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    async fn send_fake_response(prod: RedisProducer){
        let task = get_task_config();
        let stream_config = get_stream_config();
        let pconn = prod.get_paired_conn();
        let mut response = Response::new();
        response.add_result("ok".to_string(), ArgType::Bool(true));
        response.add_result("hello".to_string(), ArgType::String("world".to_string()));
        let task_response = TaskResponse::new(
            task, response, stream_config, true, None);
        let msgr = serde_json::to_string(&task_response);
        assert!(msgr.is_ok());
        let msg = msgr.ok().unwrap();
        let r = producer::publish(
                pconn, Some(1), Some(10), "test_1".to_string(), msg).await;
        assert!(r.0);
    }

    async fn setup_broker(rt: Arc<Runtime>){
        let cfg = get_config();
        let routers = get_routers();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        let broker_result = RabbitMQBroker::new(
            cfg, routers, result_store, rt).await;
        assert!(broker_result.is_ok());
        let mut broker = broker_result.ok().unwrap();
        let _r = broker.setup().await;
        broker.close().await
    }

    async fn get_send_result(
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) -> Result<AsyncResult, QueueSendError>{
        let cfg = get_config();
        let routers = get_routers();
        let broker_result = RabbitMQBroker::new(
            cfg.clone(), routers, result_store.clone(), rt).await;
        assert!(broker_result.is_ok());
        let mut broker = broker_result.ok().unwrap();
        let config = get_task_config();
        let stream = get_stream_config();
        let bck_cfg = get_backend_config();
        let r = broker.send_task(
            cfg,
            config,
            Some(stream),
            None,
            AvailableBackend::Redis((bck_cfg, 10))).await;
        broker.close().await;
        r
    }

    #[test]
    fn should_create_async_result() {
        let result = get_result();
        assert!(result.task.get_task_name().eq("test_it"))
    }

    #[test]
    fn should_complete_send(){
        let rt = get_runtime();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        rt.clone().block_on(async move {
            setup_broker(rt.clone()).await;
            let r = get_send_result(result_store, rt).await;
            let mut ar = r.unwrap();
            let sendr = ar.await_send().await;
            assert!(sendr.is_ok())
        });
    }

    #[test]
    fn should_receive_result(){
        let rt = get_runtime();
        let cfg = get_config();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        let app_status = Arc::new(AtomicBool::new(true));
        let backend_config = get_backend_config();
        let prod_config = backend_config.clone();
        let mut ids = HashSet::<String>::new();
        ids.insert("test_1".to_string());
        let new_arc = rt.clone();
        let h = rt.clone().spawn(async move {
            setup_broker(new_arc.clone()).await;
            let r = get_send_result(result_store.clone(), new_arc).await;
            assert!(r.is_ok());
            let mut ar = r.ok().unwrap();
            let prod = RedisProducer::new(prod_config).await;
            let handler = RedisResultHandler::new(
                cfg,
                app_status.clone(),
                backend_config,
                2,
                Some(ids),
                result_store.clone()
            ).await;
            let sendr = ar.await_send().await;
            assert!(sendr.is_ok());
            let _r = send_fake_response(prod).await;
            let response_result = ar.await_result().await;
            let task_response = response_result.ok().unwrap();
            let response = task_response.get_response().clone();
            let v = response.get_result("ok");
            assert!(v.is_some());
            let at = v.unwrap().clone();
            let b = match at {
                ArgType::Bool(bool) => {
                    bool.clone()
                }
                _ => {
                    false
                }
            };
            assert!(b);
            handler.close().await;
        });
        rt.block_on(async move{
           let _r = h.await;
        });
    }

    #[test]
    fn should_complete_an_entire_result(){
        let rt = get_runtime();
        let cfg = get_config();
        let app_status = Arc::new(AtomicBool::new(true));
        let mut ids = HashSet::<String>::new();
        ids.insert("test_1".to_string());
        let backend_config = get_backend_config();
        let result_store = Arc::new(RwLock::new(HashMap::new()));
        let new_arc = rt.clone();
        let handle = rt.clone().spawn(async move {
            setup_broker(new_arc.clone()).await;
            let r = get_send_result(result_store.clone(), new_arc).await;
            let mut ar = r.ok().unwrap();
            let prod = RedisProducer::new(backend_config.clone()).await;
            let handler = RedisResultHandler::new(
                    cfg,
                app_status.clone(),
                backend_config.clone(),
                2,
                Some(ids),
                result_store.clone()).await;
            send_fake_response(prod).await;
            let rtr = ar.complete().await;
            let rt = rtr.ok().unwrap();
            let b = match rt {
                ResultType::RESPONSE(rt) => {
                    let ro = rt.response;
                    if ro.is_some() {
                        let response = ro.unwrap().get_response().clone();
                        let v = response.get_result("ok");
                        assert!(v.is_some());
                        let at = v.unwrap().clone();
                        let b = match at {
                            ArgType::Bool(bool) => {
                                bool.clone()
                            }
                            _ => {
                                false
                            }
                        };
                        b
                    } else {
                        false
                    }
                }
                _ => false
            };
            assert!(b);
            handler.close().await;
        });
        rt.block_on(async move{
            let _r = handle.await;
        });
    }
}
