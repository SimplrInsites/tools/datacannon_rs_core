//! Code handler for result errors
//!
//! ---
//! author: Andrew Evans
//! ---

use std::any::Any;

use crate::error::*;

pub static OK: i8 = 0;
pub static BACKENDTYPEERROR: i8 = 1;
pub static BROKERTYPEERROR: i8 = 2;
pub static CHANNELATTAINMENTFAILED: i8 = 3;
pub static CHANNELCREATIONFAILED: i8 = 4;
pub static CONNECTIONFAILED: i8 = 5;
pub static DROPFUTUREFAILED: i8 = 6;
pub static EXCHANGEERROR: i8 = 7;
pub static FUTURECREATIONERROR: i8 = 8;
pub static POOLCREATIONERROR: i8 = 9;
pub static POOLISEMPTYERROR: i8 = 10;
pub static PUBLISHERROR: i8 = 11;
pub static QOSERROR: i8 = 12;
pub static QUEUEERROR: i8 = 13;
pub static QUEUETYPEERROR: i8 = 14;
pub static RECEIVEERROR: i8 = 15;
pub static SENDERROR: i8 = 16;
pub static SETUPERROR: i8 = 17;
pub static SSLERROR: i8 = 18;


/// Check whether the code is an actual error
///
/// # Arguments
/// * `error_code` - Code corresponding to the error type
#[allow(dead_code)]
fn is_error(error_code: i8) -> bool{
    if error_code > 0{
        true
    }else{
        false
    }
}


/// Get the error type
///
/// # Arguments
/// * `error_code` - Get the related error
#[allow(dead_code)]
fn get_error(error_code: i8)-> &'static dyn Any{
    match error_code{
        1 => &backend_type_error::BackendTypeError,
        2 => &broker_type_error::BrokerTypeError,
        3 => &channel_attainment_failed::ChannelAttainmentError,
        4 => &channel_creation_failed::ChannelCreationError,
        5 => &connection_failed::ConnectionFailed,
        6 => &drop_future_failed::DropFutureFailed,
        7 => &exchange_error::ExchangeError,
        8 => &future_creation_error::FutureCreationError,
        9 => &pool_creation_error::PoolCreationError,
        10 => &pool_creation_error::PoolCreationError,
        11 => &pool_is_empty_error::PoolIsEmptyError,
        12 => &qos_error::QOSError,
        13 => &queue_error::QueueError,
        14 => &queue_type_error::QueueTypeError,
        15 => &receive_error::QueueReceiveError,
        16 => &send_error::QueueSendError,
        17 => &setup_error::SetupError,
        18 => &ssl_error::SSLError,
        _ => &false
    }
}