//! A general message body containing chords, chains, callbacks, and other meta data
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use serde_json::{Map, to_string, Value};

use crate::AMQPProperties;
use crate::argparse::argtype::ArgType;
use crate::message_protocol::{headers::Headers, properties::Properties, stream::StreamConfig};

/// Message objects to be packaged when ready
#[derive(Clone, Debug)]
pub struct Message{
    pub properties: Properties,
    pub headers: Headers,
    pub body: StreamConfig,
    pub args: Vec<ArgType>,
    pub kwargs: HashMap<String, ArgType>,
    pub token: Option<String>,
}


/// functions for converting message to string
impl  Message{

    /// Get message parts
    pub fn get_message_parts(&self) -> (String, AMQPProperties){
        let mut props = self.properties.convert_to_amqp_properties();

        // get extra properties
        let jheaders = self.headers.convert_to_btree_map();
        props = props.with_headers(amq_protocol_types::FieldTable::from(jheaders));

        // get the message body string
        let mut body_vec = Vec::<Value>::new();

        if self.args.len() > 0 {
            let args = self.args.clone();
            let jv = serde_json::to_value(&args).ok().unwrap();
            body_vec.push(jv);
        }else{
            body_vec.push(Value::Null);
        }

        if self.kwargs.len() > 0{
            let kwargs = self.kwargs.clone();
            let jv = serde_json::to_value(kwargs).ok().unwrap();
            body_vec.push(jv);
        }else{
            body_vec.push(Value::Null);
        }

        let message_map = self.body.clone();
        let mbody_val = serde_json::to_value(message_map).ok().unwrap();
        body_vec.push(mbody_val);
        let bv = Value::Array(body_vec);
        let body_str = to_string(&bv).ok().unwrap();
        (body_str, props)
    }

    /// Convert the args, kwargs, and chain information to a bytes payload
    pub fn get_message_payload(&self) -> Vec<u8>{
        let mut payload_vec = Vec::<Value>::new();
        if self.args.len() > 0{
            let arg_vec = self.args.clone();
            let jv = serde_json::to_value(&arg_vec).ok().unwrap();
            payload_vec.push(jv);
        }else{
            let arg_vec = Vec::<Value>::new();
            payload_vec.push(Value::from(arg_vec));
        }

        if self.kwargs.len() > 0{
            let kw = self.kwargs.clone();
            let kvm =  serde_json::to_value(kw).ok().unwrap();
            payload_vec.push(kvm);
        }else{
            let kvm = Value::from(Map::new());
            payload_vec.push(kvm);
        }

        let bmap = self.body.clone();
        payload_vec.push(serde_json::to_value(bmap).ok().unwrap());

        let jstr = to_string(&payload_vec);
        if jstr.is_ok(){
            jstr.ok().unwrap().into_bytes()
        }else{
            Vec::<u8>::new()
        }
    }

    /// convert the body to json
    pub fn new(properties: Properties, headers: Headers, body: StreamConfig, args: Vec<ArgType>, kwargs: HashMap<String, ArgType>) -> Message{
        Message{
            properties: properties,
            headers: headers,
            body: body,
            args: args,
            kwargs: kwargs,
            token: None,
        }
    }
}


#[cfg(test)]
mod tests{
    use serde_json::{from_str, Value};

    use crate::argparse::argtype::ArgType;
    use crate::message_protocol::headers::Headers;
    use crate::message_protocol::properties::Properties;
    use crate::message_protocol::stream::StreamConfig;

    use super::*;

    #[test]
    fn create_new_message(){
        let correlation_id = "test_correlation";
        let content_type = "test_content";
        let content_encoding = "test_encoding";
        let props = Properties::new(correlation_id, content_type, content_encoding, None);
        let h = Headers::new("rs", "RABBITMQ","test_task", "id", "test_root");
        let mb = StreamConfig::new(None, None);
        let test_string = String::from("test");
        let arg = ArgType::String(test_string);
        let mut args = Vec::<ArgType>::new();
        args.push(arg);
        let kwargs = HashMap::<String, ArgType>::new();
        let _m = Message::new(props, h, mb, args, kwargs);
    }

    #[test]
    fn test_serialize_body(){
        let correlation_id = "test_correlation";
        let content_type = "test_content";
        let content_encoding = "test_encoding";
        let props = Properties::new(correlation_id, content_type, content_encoding, None);
        let h = Headers::new("rs", "RABBITMQ","test_task", "id", "test_root");
        let mb = StreamConfig::new(None, None);
        //let cjm = mb.convert_to_json_map();
        //let ch = cjm.get("chord");
        //let cv = ch.unwrap().to_owned();
        let test_string = String::from("test");
        let arg = ArgType::String(test_string);
        let mut args = Vec::<ArgType>::new();
        args.push(arg);
        let kwargs = HashMap::<String, ArgType>::new();
        let m = Message::new(props, h, mb, args, kwargs);
        let (body, _props) = m.get_message_parts();
        let jval = from_str(body.as_str());
        let rval: Value = jval.ok().unwrap();
        let o = rval.as_array().unwrap().to_owned();
        assert!(o.len() == 3);
        let a1 = o.get(0).unwrap().to_owned();
        let jargs = a1.as_array().unwrap().to_owned();
        assert!(jargs.len() == 1);
        assert!(jargs.get(0).unwrap().eq("test"));
        assert!(o.get(1).unwrap().to_owned() == Value::Null);
    }
}
