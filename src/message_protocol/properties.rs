//! Properties for the message
//!
//! ---
//! author: Andrew Evans
//! ---

use uuid::Uuid;

use crate::AMQPProperties;

/// Properties
///
/// # Arguments
/// * `correlation_id` - The correlation_id
/// * `content_type` - Acceptable content type
/// * `content_encoding` - Content encoding defaulting to utf-8
/// * `reply_to` - Optional queue to reply to
/// * `priority` - Queue priority
/// * `delivery_mode` - The delivery mode to use
#[derive(Clone, Debug)]
pub struct Properties{
    pub correlation_id: String,
    pub content_type: String,
    pub content_encoding: String,
    pub reply_to: Option<String>,
    pub priority: u8,
    pub delivery_mode: u8,
}


/// Properties implementation
impl Properties {

    /// Convert to `crate::message_protocol::properties::AmqProperties`
    pub fn convert_to_amqp_properties(&self) -> AMQPProperties{
        let uid =  Uuid::new_v4();
        let message_id = format!("{}", uid);
        let mut props = AMQPProperties::default();
        props = props.with_message_id(amq_protocol_types::ShortString::from(message_id));
        props = props.with_correlation_id(amq_protocol_types::ShortString::from(self.correlation_id.clone()));
        props = props.with_content_type(amq_protocol_types::ShortString::from(self.content_type.clone()));
        props = props.with_content_encoding(amq_protocol_types::ShortString::from(self.content_encoding.clone()));
        props = props.with_priority(self.priority.clone());
        props = props.with_delivery_mode(self.delivery_mode);
        if self.reply_to.is_some() {
            let rt = self.reply_to.clone().unwrap();
            props = props.with_reply_to(amq_protocol_types::ShortString::from(rt));
        }
        props
    }

    /// Create a new properties
    /// * `correlation_id` - The correlation_id
    /// * `content_type` - Acceptable content type
    /// * `content_encoding` - Content encoding defaulting to utf-8
    /// * `reply_to` - Optional queue to reply to
    /// * `priority` - Queue priority
    /// * `delivery_mode` - The delivery mode to use
    pub fn new(correlation_id: &str, content_type: &str, content_encoding: &str, reply_to: Option<&str>) -> Properties{
        let mut preply_to: Option<String> = None;
        if reply_to.is_some(){
            preply_to = Some(reply_to.unwrap().to_string());
        }
        Properties{
            correlation_id: String::from(correlation_id),
            content_type: String::from(content_type),
            content_encoding: String::from(content_encoding),
            reply_to: preply_to,
            priority: 0,
            delivery_mode: 2,
        }
    }
}


#[cfg(test)]
mod tests{
    use crate::message_protocol::properties::Properties;

    #[test]
    fn should_convert_to_amqp_properties(){
        let correlation_id = "test_correlation";
        let content_type = "test_content";
        let content_encoding = "test_encoding";
        let props = Properties::new(correlation_id, content_type, content_encoding, None);
        let aprops = props.convert_to_amqp_properties();
        assert!(aprops.correlation_id().is_some());
        assert!(aprops.correlation_id().to_owned().unwrap().as_str().eq("test_correlation"));
        assert!(aprops.content_type().is_some());
        assert!(aprops.content_type().to_owned().unwrap().as_str().eq("test_content"));
        assert!(aprops.content_encoding().is_some());
        assert!(aprops.content_encoding().to_owned().unwrap().as_str().eq("test_encoding"));
    }

    #[test]
    fn should_create_new_properties(){
        let correlation_id = "test_correlation";
        let content_type = "test_content";
        let content_encoding = "test_encoding";
        let props = Properties::new(correlation_id, content_type, content_encoding, None);
        assert!(props.correlation_id.eq("test_correlation"));
        assert!(props.content_type.eq("test_content"));
        assert!(props.content_encoding.eq("test_encoding"));
        assert!(props.reply_to.is_none());
    }
}
