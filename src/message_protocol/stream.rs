//! Message body for the broker
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};

use crate::task::config::TaskConfig;

/// Message body structure
///
/// # Arguments
/// * `chord` - Chord containing tasks to be executed at once in the same message
/// * `chain` - Chain of tasks to implement serially
/// * `callbacks` - Callback methods once completed
/// * `errbacks` - ErrHandlers for messages responding with error
#[derive(Clone, Builder, Serialize, Deserialize, Debug)]
#[builder(setter(into))]
pub struct StreamConfig{
    pub chord: Vec<TaskConfig>,
    pub chain: Vec<StreamConfig>
}



/// Implementation of StreamConfig
impl StreamConfig{

    /// Create a new message body
    /// * `chord` - Chord containing tasks to be executed at once in the same message
    /// * `chain` - Chain of tasks to implement serially
    pub fn new(chord: Option<Vec<TaskConfig>>, chain: Option<Vec<StreamConfig>>) -> StreamConfig{
        let chords = chord.unwrap_or(Vec::<TaskConfig>::new());
        let chains = chain.unwrap_or(Vec::<StreamConfig>::new());
        StreamConfig{
            chord: chords,
            chain: chains
        }
    }
}


#[cfg(test)]
mod tests{
    use std::env;

    use crate::backend::config::BackendConfig;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::message_protocol::stream::StreamConfig;
    use crate::router::router::Routers;
    use crate::task::config::TaskConfig;

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            "RABBITMQ".to_string(),
            "test_it".to_string(),
            None,
            None,
            Some("test_exchange".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            1000,
            Some(1),
            Some("rs".to_string()),
            None
        )
    }

    #[test]
    fn should_create_new_stream_config(){
        let mb = StreamConfig::new(None, None);
        assert_eq!(mb.chain.len(), 0);
    }

    #[test]
    fn should_serialize_stream_config(){
        let config = get_task();
        let sc = StreamConfig::new(None, None);
        let svec = vec![sc];
        let tvec = vec![config];
        let sc2 = StreamConfig::new(Some(tvec), Some(svec));
        let json = serde_json::to_string(&sc2);
        assert!(json.is_ok());
        println!("{}", json.ok().unwrap());
    }

    #[test]
    fn should_deserialize_stream_config(){
        let config = get_task();
        let sc = StreamConfig::new(None, None);
        let svec = vec![sc];
        let tvec = vec![config.clone()];
        let sc2 = StreamConfig::new(Some(tvec), Some(svec));
        let json_result = serde_json::to_string(&sc2);
        assert!(json_result.is_ok());
        let json = json_result.ok().unwrap();
        let stream_result = serde_json::from_str(json.as_str());
        assert!(stream_result.is_ok());
        let mut stream_config: StreamConfig = stream_result.ok().unwrap();
        assert!(stream_config.chord.len() == 1);
        assert!(stream_config.chain.len() == 1);
        assert!(stream_config.chord.pop().unwrap().get_task_name() == config.get_task_name());
    }
}

