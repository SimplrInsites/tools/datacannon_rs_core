//! Redis backend handler. Returns a channel that can be awaited on to retrieve messages.
//! One consumer subscribes to a single topic. The connection should resubscribe as needed.
//!
//! Multiple consumers can run on a single runtime. One should be started per route regsitered
//! with the application.
//!
//! ---
//! author: Andrew Evans
//! ---

use redis_async::client;
use redis_async::client::pubsub::PubsubStream;
use redis_async::client::PubsubConnection;
use redis_async::error;

use crate::backend::config::BackendConfig;
use crate::error::backend_creation_error::BackendCreationError;

/// A Redis Consumer For retrieving information from the backend
pub struct RedisConsumer{
    pub backend_conf: BackendConfig,
    conn: PubsubConnection
}


/// Implementation for the redis consumer
impl RedisConsumer{

    /// Blocking get on a pubsub conn. Sets the conn and returns success or error.
    ///
    /// # Arguments
    /// * `addr` - Address to the backend
    async fn create_pubsub_conn(addr: &str) -> Result<PubsubConnection, error::Error>{
        let socket_addr = addr.to_string().parse().unwrap();
        let conn = client::pubsub_connect(&socket_addr).await;
        conn
    }

    /// Get the pubsub stream
    ///
    /// # Arguments
    /// * `route` - The route to create a stream on
    pub async fn create_pubsub_stream(
        &mut self, route: &'static str) -> Result<PubsubStream, error::Error>{
        self.conn.subscribe(route).await
    }

    /// Obtain a cloned connection from this consumer.
    pub(crate) fn get_conn(&mut self) -> PubsubConnection{
        self.conn.clone()
    }

    /// A Redis consumer
    ///
    /// # Arguments
    /// * `backend_config` - The backend configuration
    pub async fn new(backend_config: BackendConfig) -> Result<RedisConsumer, BackendCreationError> {
        let addr = backend_config.url;
        let conn = RedisConsumer::create_pubsub_conn(addr).await;
        if conn.is_ok() {
            Ok(RedisConsumer {
                backend_conf: backend_config,
                conn: conn.ok().unwrap()
            })
        }else{
            eprintln!("Failed to Create Redis Backend: {}", conn.err().unwrap());
            Err(BackendCreationError)
        }
    }
}


#[cfg(test)]
pub mod tests{
    use tokio::runtime::Runtime;

    use crate::backend::config::BackendConfig;

    use super::*;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build()
            .ok().unwrap();
        rt
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    #[test]
    fn should_create_consumer(){
        let rt = get_runtime();
        let bc = get_backend_config();
        rt.block_on(async move {
            let consumer_result = RedisConsumer::new(bc).await;
            assert!(consumer_result.is_ok());
        });
    }
}
