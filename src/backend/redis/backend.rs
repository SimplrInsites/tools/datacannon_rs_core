//! A general rust based backend for speeding up task handling. The strategy relies on multiple named
//! consumers identified in the reply to. The parent id should be filled in as it will be the key
//! to match on. Results are stored in a HashMap that does not expect keys to be in any order.
//!
//! Map access is controlled by an arc mutex, making it atomic.
//!
//! Values in the map are conditional variables with a mutex storing the result. When a key is
//! found when a result enters the consumer, the map places the result in the mutex and notifies
//! the condvar to wake up the waiting thread. If a key does not exist, a new condvar is created.
//!
//! When checking a key in the map from the client, if it already exists it will be returned by the
//! map. If not, the client creates the key through an API call.
//!
//! All access should use the free standing functions in this library.
//!
//! The consumer is used in round robin fashion.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use futures::StreamExt;
use log::debug;
use redis_async::resp::FromResp;
use tokio::sync::{Mutex as TokioMutex, Notify, RwLock};
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;
use uuid::Uuid;

use crate::backend::config::BackendConfig;
use crate::backend::redis::consumer::RedisConsumer;
use crate::backend::redis::producer;
use crate::backend::redis::producer::RedisProducer;
use crate::broker::broker_type;
use crate::config::config::CannonConfig;
use crate::message_protocol::stream::StreamConfig;
use crate::nodename::anon_name::get_anon_nodename;
use crate::task::config::TaskConfig;
use crate::task::result::{Response, TaskResponse};

/// Stores handle information for maintenance, error checking, and control
#[derive(Clone)]
struct HandleControl{
    handle: Arc<Option<JoinHandle<()>>>,
    control: Arc<Notify>,
    route: String
}


/// Handler for Redis results. Contains a consumer and manages the stream asynchronously.
/// Requires a tokio runtime be started.
pub struct RedisResultHandler{
    config: CannonConfig<'static>,
    backend_cfg: BackendConfig,
    redis_handles: Vec<HandleControl>,
    current_handle: usize
}


/// Handler implementation
impl RedisResultHandler{

    /// Handle an incoming task, placing it in or updating the map. Returns a result containing
    /// whether new memory was created.
    ///
    /// # Arguments
    /// * `task` - The task to check
    /// * `result_store` - Result storage
    async fn handle_task(
        task: TaskResponse,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>) -> Result<bool, ()>{
        let store = result_store;
        let t = task;
        let key = t.get_task().get_parent_id();
        let wlock = store.read().await;
        let sender_opt = (*wlock).get(&key);
        if sender_opt.is_some() {
            let sender_mutex = sender_opt.unwrap();
            let sender = sender_mutex.lock().await;
            let r = sender.send(t).await;
            if r.is_ok() {
                Ok(true)
            } else {
                Err(())
            }
        }else{
            Err(())
        }
    }

    /// Asynchronously run the consumer
    ///
    /// # Arugments
    /// * `route` - The route to consume from
    /// * `backend_cfg` - The backend configuration
    /// * `app_status` - Application status to close the application
    /// * `result_store` - Result storage
    /// * `notifier` - Notify structure for when we are setup
    /// * `termination_notifier` - called on termination
    async fn run_consumer(
        route: String,
        backend_cfg: BackendConfig,
        app_status: Arc<AtomicBool>,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        notifier: Arc<Notify>,
        termination_notifier: Arc<Notify>){
        debug!("Datacannon :: core :: Subscribing redis consumer to {}", route);
        let consumer_result = RedisConsumer::new(backend_cfg).await;
        if consumer_result.is_ok(){
            let mut consumer = consumer_result.ok().unwrap();
            let psconn = consumer.get_conn();
            let msg_stream_result = psconn.subscribe(route.as_str()).await;
            if msg_stream_result.is_ok(){
                let mut msg_stream = msg_stream_result.ok().unwrap();
                notifier.notify_one();
                while app_status.load(Ordering::Relaxed){
                    let next = msg_stream.next().await;
                    if next.is_some(){
                        match next{
                            Some(Ok(next)) => {
                                let mstr_result = String::from_resp(next);
                                if mstr_result.is_ok(){
                                    let mstr = mstr_result.ok().unwrap();
                                    let task_response = serde_json::from_str(mstr.as_str());
                                    if task_response.is_ok(){
                                        let task: TaskResponse = task_response.ok().unwrap();
                                        if !task.get_task().get_task_name().eq("TERMINATE_REDIS_HANDLER"){
                                            let _r = RedisResultHandler::handle_task(
                                                task, result_store.clone()).await;

                                        }else{
                                            debug!("Datacannon :: core :: Killing Redis Consumer");
                                            app_status.store(false, Ordering::Relaxed);
                                        }
                                    }
                                }
                            },
                            _ => {}
                        }
                    }
                }
                debug!("Datacannon :: core :: Redis Consumer Stopping");
                termination_notifier.notify_one();
            }else{
                debug!("Datacannon :: core :: Failed to establish connection to redis");
                termination_notifier.notify_one();
            }
        }else{
            debug!("Datacannon :: core :: Failed to create new redis consumer");
            termination_notifier.notify_one();
        }
    }

    /// Create the result handles. Requires that the runtime is started and set.
    ///
    /// # Arguments
    /// * `app_status` - Application status
    /// * `backend_cfg` - Backend Configuration
    /// * `handle_names` - Name of each handle to create
    /// * `result_store` - Store for the results
    /// * `cpair` - Atomic boolean and condvar pairing
    async fn create_result_handles(
        app_status: Arc<AtomicBool>,
        backend_cfg: BackendConfig,
        handle_names: HashSet<String>,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>) -> Vec<HandleControl>{
        let mut handles = vec![];
        let mut notifiers = vec![];
        for name in handle_names{
            let ctl_notify = Arc::new(Notify::new());
            let notify = Arc::new(Notify::new());
            let cfg = backend_cfg.clone();
            let status = app_status.clone();
            let hname = name.clone();
            let rs = result_store.clone();
            let arc_notify = notify.clone();
            let arc_notify2 = notify.clone();
            let notified = tokio::spawn(async move{
               arc_notify2.notified().await;
            });
            notifiers.push(notified);
            let arc_ctl_notify = ctl_notify.clone();
            let handle = tokio::spawn(async move{
                RedisResultHandler::run_consumer(
                    hname,
                    cfg,
                    status,
                    rs,
                    arc_notify,
                    arc_ctl_notify).await;
            });
            let ctl = HandleControl{
                handle: Arc::new(Some(handle)),
                control: ctl_notify,
                route: name
            };
            handles.push(ctl);
        }
        for notifier in notifiers{
            let _r = notifier.await;
        }
        handles
    }

    /// Get the hostname
    ///
    /// # Arguments
    /// * `num_handles` - Number of ids to create based on the number of redis handles
    fn get_ids(num_handles: usize) -> HashSet<String>{
        let mut set = HashSet::new();
        for _i in 0..num_handles {
            let prefix = format!("{}", Uuid::new_v4());
            let host = get_anon_nodename(None, Some(prefix));
            set.insert(host);
        }
        set
    }

    /// Get the task used to terminate the loop
    fn get_termination_task(&self) -> TaskConfig{
        let config = self.config.clone();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "TERMINATE_REDIS_HANDLER".to_string(),
                        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        100000,
        None,
        None,
        None)
    }

    /// Terminate the given handle
    ///
    /// # Arguments
    /// * `ctl` - Control containing handle and other variables
    async fn terminate_handle(&self, ctl: HandleControl){
        let route = ctl.route;
        let cfg = self.backend_cfg.clone();
        let task = self.get_termination_task();
        let response = Response::new();
        let stream = StreamConfig::new(None, None);
        let tresp = TaskResponse::new(task,response,stream, false, None);
        let rproducer = RedisProducer::new(cfg).await;
        let pconn = rproducer.get_paired_conn();
        let msg_result = serde_json::to_string(&tresp);
        let arc_notify = ctl.control.clone();
        let handle = tokio::spawn(async move{
            arc_notify.notified().await;
        });
        if msg_result.is_ok() {
            let msg = msg_result.ok().unwrap();
            let r = producer::publish(
                pconn, Some(1), Some(100), route, msg).await;
            if r.0 == false{
                panic!("Failed to Shutdown Redis Handler");
            }else{
                let _r = handle.await;
            }
        }else{
            panic!("Failed to Shutdown Redis Handler");
        }
    }

    /// Close the consumers. Will consume variables and render the object useless.
    pub async fn close(&self){
        for i in 0..self.redis_handles.len(){
            let ctl = self.redis_handles.get(i).unwrap().clone();
            self.terminate_handle(ctl).await;
        }
    }

    /// Get the current handle name for the reply to
    pub fn get_handle_name(&mut self) -> Result<String, ()>{
        let handle = self.redis_handles.get(self.current_handle);
        if handle.is_some() {
            self.current_handle += 1;
            Ok(handle.unwrap().route.clone())
        }else{
            Err(())
        }
    }

    /// Create a new backend handler
    ///
    /// # Arguments
    /// * `app_status` - Application status
    /// * `backend_cfg` - The backend configuration
    /// * `num_handles` - Number of handles
    /// * `ids` - Optional set of ids, will create one. Must be unique in the entire system
    pub async fn new(
        config: CannonConfig<'static>,
        app_status: Arc<AtomicBool>,
        backend_cfg: BackendConfig,
        num_handles: usize,
        ids: Option<HashSet<String>>,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>) -> RedisResultHandler{
        let route_ids = {
            if ids.is_some(){
                ids.unwrap()
            }else{
                RedisResultHandler::get_ids(num_handles)
            }
        };
        let handles = RedisResultHandler::create_result_handles(
            app_status,
            backend_cfg.clone(),
            route_ids,
            result_store).await;
        RedisResultHandler{
            config,
            backend_cfg,
            redis_handles: handles,
            current_handle: 0
        }
    }
}

#[cfg(test)]
pub mod tests{
    use std::collections::HashSet;
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use tokio::runtime::Runtime;

    use crate::backend::config::BackendConfig;
    use crate::backend::redis::producer;
    use crate::backend::redis::producer::RedisProducer;
    use crate::broker::broker_type;
    use crate::config::config::BackendType;
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::message_protocol::stream::StreamConfig;
    use crate::router::router::Routers;
    use crate::task::config::TaskConfig;
    use crate::task::result::{Response, TaskResponse};

    use super::*;

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(10)
            .build().unwrap();
        Arc::new(rt)
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }
    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_task_config(reply_to: String) -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some(reply_to),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_task_response() -> TaskResponse{
        let task_config = get_task_config("test_reply".to_string());
        let response = Response::new();
        let stream = StreamConfig::new(None, None);
        let response = TaskResponse::new(task_config, response, stream, false, None);
        response
    }

    async fn send_response_through_redis(route: String){
        let backend_config = get_backend_config();
        let redis_producer = RedisProducer::new(backend_config).await;
        let pconn = redis_producer.get_paired_conn();
        let task_response = get_task_response();
        let msg_result = serde_json::to_string(&task_response);
        assert!(msg_result.is_ok());
        let msg = msg_result.ok().unwrap();
        producer::publish(pconn, Some(1), Some(100), route, msg).await;
    }

    #[test]
    fn should_create_results_backend(){
        let cfg = get_config();
        let rt = get_runtime();
        let results = Arc::new(RwLock::new(HashMap::new()));
        let backend = get_backend_config();
        let num_handles = 3;
        let app_status = Arc::new(AtomicBool::new(true));
        rt.block_on(async move {
            let handler = RedisResultHandler::new(
                cfg,app_status.clone(), backend, num_handles, None, results.clone()).await;
            app_status.store(false, Ordering::Relaxed);
            handler.close().await;
        });
    }

    #[test]
    fn should_check_results_backend(){
        let rt = get_runtime();
        let cfg = get_config();
        let h = rt.spawn(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let backend = get_backend_config();
            let num_handles = 3;
            let app_status = Arc::new(AtomicBool::new(true));
            let handler = RedisResultHandler::new(
                cfg,
                app_status.clone(),
                backend,
                num_handles,
                None,
                results.clone()).await;
            let arc_results = results.clone();
            let rlock = arc_results.read().await;
            let mval = rlock.get("dne");
            assert!(mval.is_none());
            handler.close().await;
        });
        rt.block_on(async move{
            let _r = h.await;
        });
    }

    #[test]
    fn should_add_result_from_redis(){
        let rt = get_runtime();
        let cfg = get_config();
        let handle = rt.spawn(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let backend_config = get_backend_config();
            let app_status = Arc::new(AtomicBool::new(true));
            let mut ids = HashSet::<String>::new();
            ids.insert("test_1".to_string());
            ids.insert("test_2".to_string());
            let handler = RedisResultHandler::new(
                cfg,
                app_status.clone(),
                backend_config,
                2,
                Some(ids),
                results.clone()).await;
            let (sender, mut receiver) = tokio::sync::mpsc::channel(1);
            let msender = Arc::new(TokioMutex::new(sender));
            let aresults = results.clone();
            let h = tokio::spawn(async move {
                let mut wlock = aresults.write().await;
                wlock.insert("test_parent_id".to_string(), msender);
            });
            let _r = h.await;
            send_response_through_redis("test_1".to_string()).await;
            let h = tokio::spawn(async move {
                let _r = receiver.recv().await;
            });
            let _r = h.await;
            app_status.store(false, Ordering::Relaxed);
            handler.close().await;
        });
        rt.block_on(async move{
           let _r = handle.await;
        });
    }
}