//! Redis backend handler. Returns a consumer which subscribes to
//! a queue.
//!
//! ---
//! author: Andrew Evans
//! ---

use redis_async::{client, error, resp_array};
use redis_async::client::PairedConnection;
use redis_async::resp::RespValue;

use tokio::time::{self, Duration};

use crate::backend::config::BackendConfig;

/// A Redis Producer. Pushes messages back to redis.
#[derive(Clone, Debug)]
pub struct RedisProducer{
    backend_conf: BackendConfig,
    paired_conn: PairedConnection,
}


/// Implementation of the Redis Producer
impl RedisProducer{

    /// Return a reference to the paired connection attached to this object
    #[allow(dead_code)]
    pub fn get_paired_conn(&self) -> PairedConnection{
        self.paired_conn.clone()
    }

    ///Get a reference to the backend config
    pub fn get_backend_config(&self) -> &BackendConfig{
       &self.backend_conf
    }

    /// Get a pubsub connection
    async fn create_paired_conn(addr: &str) -> PairedConnection{
        let url = addr.parse().unwrap();
        let paired_result = client::paired_connect(&url).await;
        debug_assert_eq!(paired_result.is_ok(), true);
        paired_result.ok().unwrap()
    }

    /// Create a new redis producer. Returns a `crate::backend::redis::producer::RedisProducer`
    ///
    /// # Arguments
    /// * `conf` - The backend configuration
    pub async fn new(conf: BackendConfig) -> RedisProducer{
        let addr = conf.url;
        let conn = RedisProducer::create_paired_conn(addr).await;
        RedisProducer{
            backend_conf: conf,
            paired_conn: conn
        }
    }
}


/// Publish messages to the given channel for an optional number of attempts. The connection
/// causes send attempts to fail when re-establishing. Therefore a retries and retry wait
/// nanoseconds are available. Make sure to provide an adequate number of nanoseconds if not
/// relying on the default.
///
/// # Arguments
/// * `paired_conn` - The Paired Connection to execute on
/// * `retries` - Optional number of retry attempts
/// * `retry_wait` - Nanoseconds to wait between retries
/// * `route` - Name of the route to send to
/// * `message` - Clonable message string
pub async fn publish(
    paired_conn: PairedConnection, retries: Option<u8>, retry_wait: Option<u32>, route: String, message: String) -> (bool, u8){
    let max_retries = retries.unwrap_or(1);
    let retry_wait = retry_wait.unwrap_or(500000);
    let mut retries = 0;
    let retry_duration = Duration::new(0, retry_wait);
    let mut sval: Result<RespValue, error::Error> = paired_conn.send(resp_array!["PUBLISH", route.as_str(), message.as_str()]).await;
    while sval.is_err() && retries < max_retries{
        time::sleep(retry_duration).await;
        sval = paired_conn.send(resp_array!["PUBLISH", route.as_str(), message.as_str()]).await;
        retries += 1;
    }
    if sval.is_ok(){
        (true, retries)
    }else{
        (false, retries)
    }
}


#[cfg(test)]
pub mod tests{
    use std::sync::{Arc, Condvar, Mutex};
    use std::time::Duration;

    use futures::StreamExt;
    use redis_async::resp::FromResp;
    use tokio::runtime::Runtime;
    use tokio::sync::Semaphore;

    use crate::backend::config::BackendConfig;

    use super::*;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .enable_all().build().ok().unwrap();
        rt
    }

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    #[test]
    fn should_push_and_pull_from_redis_over_paired_conn(){
        let rt = get_runtime();
        let bc = get_backend_config();
        rt.block_on(async move {
            let redis = RedisProducer::new(bc).await;
            let _pc = redis.paired_conn;
        });
    }

    #[test]
    fn send_messages_to_redis(){
        let rt = get_runtime();
        let bc = get_backend_config();
        let addr = bc.url;
        rt.block_on(async move {
            let redis = RedisProducer::new(bc).await;
            let pc = redis.paired_conn;
            let cvar = Arc::new((Mutex::new(false), Condvar::new()));
            let cvar1 = cvar.clone();
            let cvar2 = cvar.clone();
            let pcb = pc.clone();
            let mut pub_r = pcb.send::<String>(resp_array!["SET", "test_key2".to_string(), "test_m".to_string()]).await;
            assert!(pub_r.is_ok());
            pub_r = pcb.send::<String>(resp_array!["GET", "test_key2".to_string()]).await;
            assert!(pub_r.is_ok());
            assert!(pub_r.ok().unwrap().eq("test_m"));
            let sema = Arc::new(Semaphore::new(1));
            let sema1 = sema.clone();
            let sema2 = sema.clone();
            let saddr = addr.parse().unwrap();
            let ps_connr = client::pubsub_connect(&saddr).await;
            assert!(ps_connr.is_ok());
            let ps_conn = ps_connr.ok().unwrap();
            let ps_conn2 = ps_conn.clone();
            let h = tokio::spawn(async move {
                let permit = sema1.acquire().await.unwrap();
                permit.forget();
                let msgs = ps_conn2.subscribe("test_route").await;
                assert!(msgs.is_ok());
                sema.add_permits(1);
                {
                    let (lock, cond) = &*cvar1;
                    let mut status = lock.lock().unwrap();
                    *status = true;
                    cond.notify_all();
                }
                let mut msg_stream = msgs.ok().unwrap();
                if let Some(message) = msg_stream.next().await {
                    match message {
                        Ok(message) => {
                            let mstr = String::from_resp(message).unwrap();
                            println!("{}", mstr);
                            assert!(mstr.eq("test_m"));
                        },
                        Err(e) => {
                            eprintln!("ERROR: {}", e);
                            assert!(false);
                        }
                    }
                } else {
                    assert!(false);
                }
            });

            let h2 = tokio::spawn(async move {
                {
                    let (lock, cond) = &*cvar2;
                    let status = lock.lock().unwrap();
                    let _r = cond.wait_timeout(status, Duration::new(0, 100));
                }
                let permit = sema2.acquire().await.unwrap();
                permit.forget();
                let (is_ok, _r) = publish(
                    pc.clone(), Some(1), Some(2), "test_route".to_string(), "test_m".to_string()).await;
                assert!(is_ok);
            });
            let _r1 = h.await;
            let _r2 = h2.await;
            ps_conn.unsubscribe("test_route");
        });
    }
}
