/// Stores backend with their configs
///
/// ---
/// author: Andrew Evans
/// ---

use crate::backend::config::BackendConfig;

/// The backend types with configuration storage
#[derive(Clone, Debug)]
pub enum AvailableBackend {
    Redis((BackendConfig, u8)),
    NONE
}