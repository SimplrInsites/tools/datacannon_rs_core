//! Configuration for backends
//!
//! ---
//! author: Andrew Evans
//!

#[derive(Clone, Builder, Debug)]
#[builder(setter(into))]
pub struct BackendConfig{
    pub url: &'static str,
    pub username: Option<&'static str>,
    pub password: Option<&'static str>,
    pub transport_options: Option<&'static str>
}
