//! For creating and maintaining connection information
//!
//! ---
//! author: Andrew Evans
//! ---

use lapin::Connection;

use crate::connection::amqp::connection_inf::AMQPConnectionInf;
use crate::connection::amqp::rabbitmq_connection_utils;
use crate::error::connection_failed::ConnectionFailed;
use tokio::runtime::Runtime;
use std::sync::Arc;

/// Connection object containing a connection and a channel
pub struct RabbitMQConnection{
    pub connection: Connection,
}


///RabbitMQ connection implmentation
impl RabbitMQConnection{

    /// Create a connection asynchronously
    ///
    /// # Arguments
    /// * `conn_inf` - The connection information
    pub async fn new(
        conn_inf: &AMQPConnectionInf) -> Result<RabbitMQConnection, ConnectionFailed>{
        let conn_res = rabbitmq_connection_utils::get_connection(
            conn_inf).await;
        if conn_res.is_ok(){
            let conn = conn_res.ok().unwrap();
            let rmq = RabbitMQConnection{
                connection: conn,
            };
            Ok(rmq)
        }else{
            Err(ConnectionFailed)
        }
    }
}


#[cfg(test)]
pub mod test{
    use std::env;

    use tokio::runtime::Runtime;

    use crate::connection::amqp::connection_inf::AMQPConnectionInf;

    use super::*;

    fn get_runtime() -> Runtime{
        let rtr = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4).build();
        rtr.unwrap()
    }


    #[test]
    fn should_create_connection(){
        let rt = get_runtime();
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let art = Arc::new(rt);
        let closed = art.clone().block_on(async move{
            let conn = RabbitMQConnection::new(&amq_conf).await;
            assert!(conn.is_ok());
            conn.unwrap().connection.close(200, "").await
        });
        assert!(closed.is_ok())
    }

    #[test]
    fn should_return_error_on_failure(){
        let rt = get_runtime();
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5675,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let art = Arc::new(rt);
        art.clone().block_on(async move {
            let conn = RabbitMQConnection::new(&amq_conf).await;
            assert!(conn.is_err());
        });
    }
}