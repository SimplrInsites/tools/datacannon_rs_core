//! RabbitMQ Connection Pool
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use lapin::Channel;
use tokio::sync::{Mutex, RwLock};

use crate::config::config::CannonConfig;
use crate::connection::amqp::rabbitmq_connection::RabbitMQConnection;
use crate::connection::connection::ConnectionConfig;
use crate::error::channel_creation_failed::ChannelCreationError;
use crate::error::pool_creation_error::PoolCreationError;
use tokio::runtime::Runtime;

/// RabbitMQ Connection Pool
pub struct RabbitMQConnectionPool{
    connections: Arc<Mutex<Vec<RabbitMQConnection>>>,
    current_connection: Arc<RwLock<usize>>,
}

/// Implementation of the RabbitMQ Connection Pool
impl RabbitMQConnectionPool{

    /// Drop all connections
    async fn drop_connections(&mut self) {
        let conns = self.connections.clone();
        let current = self.current_connection.clone();
        let mut mgaurd = conns.lock().await;
        while mgaurd.len() > 0{
            let conn: RabbitMQConnection = mgaurd.pop().unwrap();
            let _r = conn.connection.close(200, "Complete").await;
        }
        let mut wlock = current.write().await;
        *wlock = 0;
    }

    /// Close the pool
    pub async fn close(&mut self) {
        self.drop_connections().await;
    }

    /// Get the size of the pool in `std::usize`
    pub async fn get_pool_size(&mut self) -> usize{
        let conn_arc = self.connections.clone();
        let conn_gaurd = conn_arc.lock().await;
        conn_gaurd.len().clone()
    }

    /// Get the channel
    pub async fn get_channel(&mut self) -> Result<Channel, ChannelCreationError>{
        let conns = self.connections.clone();
        let current = self.current_connection.clone();
        let csize = current.clone();
        let rlock = csize.read().await;
        let idx = (*rlock).clone();
        drop(rlock);
        let conn_gaurd = conns.lock().await;
        let c: &RabbitMQConnection = conn_gaurd.get(
            idx).unwrap();
        let channel_result = c.connection.create_channel().await;
        if channel_result.is_ok(){
            let mut wlock = current.write().await;
            if idx + 1 >= conn_gaurd.len(){
                *wlock = 0
            }else{
                *wlock = idx + 1;
            }
            Ok(channel_result.unwrap())
        }else{
            let mut wlock = current.write().await;
            if idx + 1 >= conn_gaurd.len(){
                *wlock = 0
            }else{
                *wlock = idx + 1;
            }
            Err(ChannelCreationError)
        }
    }

    /// Create the pool
    ///
    /// # Arguments
    /// * `cannon_config` - Application configuration
    /// * `is_broker` - Whether this is for a broker
    pub async fn create(cannon_config: CannonConfig<'static>, is_broker: bool) -> Result<RabbitMQConnectionPool, PoolCreationError> {
        let mut conn_vec = Vec::<RabbitMQConnection>::new();
        let mut num_conn = cannon_config.num_broker_connections;
        if is_broker == false{
            num_conn = cannon_config.num_backend_connections;
        }
        let conn_inf = cannon_config.connection_inf.clone();
        if let ConnectionConfig::RabbitMQ(conn_inf) = conn_inf {
            for _i in 0..num_conn {
                let c = RabbitMQConnection::new(&conn_inf).await;
                if c.is_ok(){
                    conn_vec.push(c.ok().unwrap());
                }
            }
            if conn_vec.len() == num_conn as usize {
                let rmq = RabbitMQConnectionPool {
                    connections: Arc::new(Mutex::new(conn_vec)),
                    current_connection: Arc::new(RwLock::new(0)),
                };
                Ok(rmq)
            }else{
                Err(PoolCreationError)
            }
        }else{
            Err(PoolCreationError)
        }
    }

    /// Create a new RabbitMQ Connection Pool. Setup creates the connections
    ///
    /// # Arguments
    /// * `cannon_config` - The application `crate::config::config::CannonConfig`
    /// * `is_broker` - Whether the connection pool belongs to a broker
    pub async fn new(
        cannon_config: &CannonConfig<'static>,  is_broker: bool) -> Result<RabbitMQConnectionPool, PoolCreationError> {
        let mut conn_vec = Vec::<RabbitMQConnection>::new();
        let mut num_conn = cannon_config.num_broker_connections;
        if is_broker == false{
            num_conn = cannon_config.num_backend_connections;
        }
        let conn_inf = cannon_config.connection_inf.clone();
        if let ConnectionConfig::RabbitMQ(conn_inf) = conn_inf {
            for _i in 0..num_conn {
                let c = RabbitMQConnection::new(&conn_inf).await;
                if c.is_ok(){
                    conn_vec.push(c.ok().unwrap());
                }
            }
            if conn_vec.len() == num_conn as usize {
                let rmq = RabbitMQConnectionPool {
                    connections: Arc::new(Mutex::new(conn_vec)),
                    current_connection: Arc::new(RwLock::new(0)),
                };
                Ok(rmq)
            }else{
                Err(PoolCreationError)
            }
        }else{
            Err(PoolCreationError)
        }
    }
}


#[cfg(test)]
pub mod test{
    use std::{env};

    use tokio::runtime::Runtime;

    use crate::backend::config::BackendConfig;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::amqp::rabbit_mq_connection_pool::RabbitMQConnectionPool;
    use crate::connection::connection::ConnectionConfig;
    use crate::error::pool_creation_error::PoolCreationError;
    use crate::router::router::Routers;
    use std::sync::Arc;

    async fn do_get_rmq_pool(rt: Arc<Runtime>) -> Result<RabbitMQConnectionPool, PoolCreationError>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_connections = 1;
        let rmq_pool = RabbitMQConnectionPool::new(
            &cannon_conf,  true).await;
        rmq_pool
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2).enable_all().build().unwrap();
        Arc::new(rt)
    }

    #[test]
    fn should_create_connection_pool(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let pool_res = do_get_rmq_pool(rt).await;
            assert!(pool_res.is_ok());
            let _r = pool_res.unwrap().close().await;
        });
    }

    #[test]
    fn should_setup_pool(){
        let rt = get_runtime();
        rt.clone().block_on(async move{
            let pool_res = do_get_rmq_pool(rt).await;
            assert!(pool_res.is_ok());
            let mut pool = pool_res.ok().unwrap();
            pool.close().await;
        });
    }

    #[test]
    fn should_close_pool(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let pool_res = do_get_rmq_pool(rt).await;
            assert!(pool_res.is_ok());
            let mut pool = pool_res.ok().unwrap();
            pool.close().await;
            assert!(pool.get_pool_size().await == 0);
        });
    }

    #[test]
    fn should_get_channel(){
        let rt = get_runtime();
        rt.clone().block_on(async move{
            let pool_res = do_get_rmq_pool(rt).await;
            assert!(pool_res.is_ok());
            let mut pool = pool_res.ok().unwrap();
            let ch = pool.get_channel().await;
            pool.close().await;
            assert!(ch.is_ok());
            assert!(pool.get_pool_size().await == 0);
        });
    }

    #[test]
    fn should_fail_gracefully() {
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let pool_res = do_get_rmq_pool(rt).await;
            assert!(pool_res.is_ok());
            let mut pool = pool_res.ok().unwrap();
            pool.close().await;
            assert!(pool.get_pool_size().await == 0);
        });
    }
}
