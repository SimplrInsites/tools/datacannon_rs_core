//! AMQP Connection utilities
//!
//! ---
//! author: Andrew Evans
//! ---

use std::panic;

/// Struct for connection information
///
/// # Arguments
/// * `protocol` - Protocol to use such as amqp
/// * `host` - Host &'a str
/// * `port` - AMQP connection port
/// * `vhost` - Virtual host name of necessary
/// * `username` - Username if necessary and usually provided with a password
/// * `password` - Password if necessary and usually provided with a username
/// * `is_ssl` - Whether to use ssl
/// * `ssl_config` - SSL configuration if necessary
/// * `uaa_config` - Connection information for an OAuth UAA server
#[derive(Clone, Debug)]
pub struct AMQPConnectionInf{
    protocol: String,
    host: String,
    port: i64,
    vhost: Option<String>,
    username: Option<String>,
    password: Option<String>,
    connection_timeout: u64
}


/// Implementation of the connection information
impl AMQPConnectionInf{

    /// Get the protocol `std::&'a str::&'a str`
    pub fn get_protocol(&self) -> &str{
        self.protocol.as_str()
    }

    ///Get the host `std::&'a str::&'a str`
    pub fn get_host(&self) -> &str{
        self.host.as_str()
    }

    /// Get the port  `std::i64`
    pub fn get_port(&self) -> i64{
        self.port.clone()
    }

    /// Get the vhost `std::option::Option::<std::&'a str::&'a str>`
    pub fn get_vhost(&self) -> Option<String>{
        self.vhost.clone()
    }

    /// Get the username `std::option::Option<std::&'a str::&'a str>`
    pub fn get_username(&self) -> Option<String>{
        self.username.clone()
    }

    /// Get the password `std::option::Option<std::&'a str::&'a str>`
    pub fn get_password(&self) -> Option<String>{
        self.password.clone()
    }

    /// Get the connection timeout
    pub fn get_conection_timeout(&self) -> u64{
        self.connection_timeout.clone()
    }

    /// convert the Information to a URL `std::&'a str::&'a str`
    #[allow(unused_assignments)]
    pub fn to_url(&self) -> String {
        let cinf = self.clone();
        let mut url: Option<String> = None;
        if cinf.username.is_some() && cinf.password.is_some(){
            url = Some(format!("{}://{}:{}@{}:{}", cinf.protocol, cinf.username.unwrap(), cinf.password.unwrap(), cinf.host, cinf.port));
        }else{
            url = Some(format!("{}://{}:{}", cinf.protocol, cinf.host, cinf.port));
        }
        if self.vhost.is_some(){
            url = Some(format!("{}/{}", url.unwrap(), cinf.vhost.unwrap()));
        }
        if url.is_some() {
            url.unwrap()
        }else{
            panic!("URL For Connection Could Not Be Generated")
        }
    }

    /// Create a new connection information structure
    ///
    /// # Arguments
    /// * `protocol` - Protocol to use such as amqp
    /// * `host` - Host &'a str
    /// * `port` - AMQP connection port
    /// * `vhost` - Virtual host name of necessary
    /// * `username` - Username if necessary and usually provided with a password
    /// * `password` - Password if necessary and usually provided with a username
    /// * `is_ssl` - Whether to use ssl
    /// * `ssl_config` - SSL configuration if necessary
    /// * `uaa_config` - Connection information for an OAuth UAA server
    pub fn new(
        protocol: String,
        host: String,
        port: i64,
        vhost: Option<String>,
        username: Option<String>,
        password: Option<String>,
        connection_timeout: u64) -> AMQPConnectionInf{
        AMQPConnectionInf{
            protocol: protocol,
            host: host,
            port: port,
            vhost: vhost,
            username: username,
            password: password,
            connection_timeout: connection_timeout
        }
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    pub fn test_create_url(){
        let cinf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            3030,
            None,
            None,
            None,
            10000);
        let url = cinf.to_url();
        assert!(url.eq("amqp://127.0.0.1:3030"));
    }

    #[test]
    pub fn should_use_credentials(){
        let cinf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            3030,
            None,
            Some("test".to_string()),
            Some("123".to_string()),
            10000);
        let url = cinf.to_url();
        assert!(url.eq("amqp://test:123@127.0.0.1:3030"));
    }
}