//! A channel pool
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering};

use crossbeam::queue::ArrayQueue;
use lapin::Channel;

use crate::connection::amqp::rabbit_mq_connection_pool::RabbitMQConnectionPool;

/// Channel Pool Structure
pub(crate) struct RabbitMQChannelPool{
    available_channels: Arc<ArrayQueue<Channel>>,
    current_num_channels: AtomicUsize
}


/// Channel Pool implementation
impl RabbitMQChannelPool{

    /// Get the current number of channels atomically
    #[allow(dead_code)]
    pub(crate) fn get_current_num_channels(&mut self) -> usize{
        self.current_num_channels.load(Ordering::Acquire)
    }

    /// Pops a channel wrapped in an option if available and connected.
    /// Returns None otherwise. Users must add a channel again.
    pub(crate) fn get_channel(&mut self) -> Option<Channel>{
        let r  = self.available_channels.as_ref();
        let ch_opt = r.pop();
        if ch_opt.is_some(){
            *self.current_num_channels.get_mut() -= 1;
            let ch: Channel = ch_opt.unwrap();
            if ch.status().connected(){
                Some(ch)
            }else {
                None
            }
        }else {
            None
        }
    }

    /// Must be called. pushes a channel back ont the vector.
    /// Returns whether the channel was added back to the pool.
    ///
    /// # Arguments
    /// * `channel` - The Channel to use
    pub(crate) fn release(&mut self, channel: Channel) -> bool{
        if channel.status().connected() {
            let _r = self.available_channels.as_ref().push(channel);
            *self.current_num_channels.get_mut() += 1;
            true
        }else{
            false
        }
    }

    /// Add new channels
    ///
    /// # Arguments
    /// * `num_channels` - The number of channels to add
    /// * `conn_pool` - The connection pool to use
    pub(crate) async fn add_channels(&mut self, num_channels: usize, conn_pool: &mut RabbitMQConnectionPool){
        for _i in 0..num_channels {
            let curr_channel = conn_pool.get_channel().await.unwrap();
            let _r = self.available_channels.as_ref().push(curr_channel);
        }
        *self.current_num_channels.get_mut() += num_channels;
    }

    /// Asynchronously create a channel pool
    ///
    /// # Arguments
    /// * `num_channels` - Number of channels to add
    /// * `conn_pool` - The RabbitMQConnectionPool to round robin on
    pub(crate) async fn create(
        num_channels: usize, conn_pool: &mut RabbitMQConnectionPool) -> RabbitMQChannelPool {
        let channel_q = ArrayQueue::<Channel>::new(num_channels);
        for _i in 0..num_channels.clone(){
            let curr_channel = conn_pool.get_channel().await.unwrap();
            let _r = channel_q.push(curr_channel);
        }
        RabbitMQChannelPool{
            available_channels: Arc::new(channel_q),
            current_num_channels: AtomicUsize::new(num_channels)
        }
    }
}


#[cfg(test)]
mod tests{
    use std::env;
    use std::sync::Mutex;

    use amq_protocol_types::FieldTable;
    use lapin::BasicProperties;
    use lapin::ExchangeKind;
    use lapin::options::*;
    use tokio::runtime::Runtime;
    use tokio::sync::Semaphore;
    use tokio::task::JoinHandle;

    use crate::backend::config::BackendConfig;
    use crate::config::config::{BackendType, CannonConfig};
    use crate::connection::amqp::connection_inf::AMQPConnectionInf;
    use crate::connection::connection::ConnectionConfig;
    use crate::router::router::Routers;

    use super::*;

    /// Acquire the config for testing
    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let cannon_conf = CannonConfig::new(conn_conf,
                                            BackendType::REDIS, backend_conf, routers);
        cannon_conf
    }

    /// Sets up globals
    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(5)
            .thread_name("test")
            .thread_stack_size(3 * 1024 * 1024)
            .enable_time()
            .build()
            .unwrap();
        Arc::new(rt)
    }

    /// Get the connection pool
    ///
    /// # Arguments
    /// * `pool` - The relevant RabbitMQ Connection Pool
    async fn get_channel_pool(pool: &mut RabbitMQConnectionPool) -> RabbitMQChannelPool{
        RabbitMQChannelPool::create(5,pool).await
    }

    /// Get the connection pool
    ///
    /// # Arguments
    /// * `pool` - The relevant RabbitMQ Connection Pool
    async fn get_single_channel_pool(pool: &mut RabbitMQConnectionPool) -> RabbitMQChannelPool{
        RabbitMQChannelPool::create(1,pool).await
    }

    ///Obtain the connection pool to create teh channel pool with
    async fn get_connection_pool(rt: Arc<Runtime>) -> RabbitMQConnectionPool{
        let cfg = get_config();
        let pl = RabbitMQConnectionPool::new(
            &cfg, true).await;
        pl.ok().unwrap()
    }

    #[test]
    fn should_create_pool(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut _chp = get_channel_pool(&mut pool);
        });
    }

    #[test]
    fn should_acquire_a_channel(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut chp = get_channel_pool(&mut pool).await;
            let ch = chp.get_channel();
            assert!(ch.is_some());
        });
    }

    #[test]
    fn should_release_channel(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut chp = get_channel_pool(&mut pool).await;
            let ch = chp.get_channel();
            assert!(ch.is_some());
            chp.release(ch.unwrap());
        });
    }

    #[test]
    fn should_add_new_channels(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut chp = get_channel_pool(&mut pool).await;
            let ch = chp.get_channel();
            assert!(ch.is_some());
            chp.add_channels(1, &mut pool).await;
        });
    }

    #[test]
    fn should_reacquire_channel(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut chp = get_single_channel_pool(&mut pool).await;
            let ch = chp.get_channel();
            assert!(ch.is_some());
            chp.add_channels(1, &mut pool).await;
            let ch2 = chp.get_channel();
            assert!(ch2.is_some());
        });
    }

    #[test]
    fn should_track_available_channels(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let mut pool = get_connection_pool(rt).await;
            let mut chp = get_channel_pool(&mut pool).await;
            let _ch = chp.get_channel().unwrap();
            let _ch2 = chp.get_channel().unwrap();
            assert!(chp.get_current_num_channels() == 3);
            chp.add_channels(3, &mut pool).await;
            assert!(chp.get_current_num_channels() == 6);
        });
    }

    #[test]
    fn should_reacquire_and_reuse_connections(){
        let runtime = get_runtime();
        let new_arc = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpool = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpool.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = ExchangeDeclareOptions::default();
                excopts.nowait = false;
                excopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                let confirm = ch.exchange_declare(
                    "my_exchange",
                    ExchangeKind::Direct,
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpool.lock().unwrap();
                    charc.release(ch);
                }
            }
        });
        let new_arc2 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc2).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolb = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolb.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = ExchangeDeclareOptions::default();
                excopts.nowait = false;
                excopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.exchange_declare(
                    "my_exchange2",
                    ExchangeKind::Direct,
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolb.lock().unwrap();
                    charc.release(ch);
                }
            }
        });
        let new_arc3 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc3).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolc = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolc.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = ExchangeDeclareOptions::default();
                excopts.nowait = false;
                excopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.exchange_declare(
                    "my_exchange3",
                    ExchangeKind::Direct,
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolc.lock().unwrap();
                    charc.release(ch);
                }
            }
        });
        for i in 0..2 {
            let new_arc4 = runtime.clone();
            runtime.block_on(async move {
                let mut conn_pool = get_connection_pool(new_arc4).await;
                let pool = get_single_channel_pool(&mut conn_pool).await;
                let ch_pool = Arc::new(Mutex::new(pool));
                let rpoold = ch_pool.clone();
                let ch_opt = {
                    let mut charc = rpoold.lock().unwrap();
                    charc.get_channel()
                };
                if ch_opt.is_some() {
                    let mut excopts = ExchangeDeclareOptions::default();
                    excopts.nowait = false;
                    excopts.durable = true;
                    let ftable = FieldTable::default();
                    let ch = ch_opt.unwrap();
                    assert!(ch.status().connected());
                    let confirm = ch.exchange_declare(
                        format!("my_exchange_{}", i).as_str(),
                        ExchangeKind::Direct,
                        excopts,
                        ftable
                    ).await;
                    assert!(confirm.is_ok());
                    {
                        let mut charc = rpoold.lock().unwrap();
                        charc.release(ch);
                    }
                }
            });
        }
    }

    #[test]
    fn should_create_exchange_queues_bind_and_pub_on_single_channel_pool(){
        let runtime = get_runtime();
        let new_arc = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpool = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpool.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = ExchangeDeclareOptions::default();
                excopts.nowait = false;
                excopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                let confirm = ch.exchange_declare(
                    "my_exchange",
                    ExchangeKind::Direct,
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpool.lock().unwrap();
                    charc.release(ch);
                }
            }else{
                assert!(false);
            }
        });
        let new_arc2 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc2).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolb = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolb.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut qopts = QueueDeclareOptions::default();
                qopts.nowait = false;
                qopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.queue_declare(
                    "my_queue",
                    qopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolb.lock().unwrap();
                    charc.release(ch);
                }
            }else{
                assert!(false);
            }
        });
        let new_arc4 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc4).await;
            let pool = get_single_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolc = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolc.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = QueueBindOptions::default();
                excopts.nowait = false;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.queue_bind(
                    "my_queue",
                    "my_exchange",
                    "test_route",
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolc.lock().unwrap();
                    charc.release(ch);
                }
            }
        });
        for i in 0..2 {
            let new_arc5 = runtime.clone();
            runtime.block_on(async move {
                let mut conn_pool = get_connection_pool(new_arc5).await;
                let pool = get_single_channel_pool(&mut conn_pool).await;
                let ch_pool = Arc::new(Mutex::new(pool));
                let rpoold = ch_pool.clone();
                let ch_opt = {
                    let mut charc = rpoold.lock().unwrap();
                    charc.get_channel()
                };
                if ch_opt.is_some() {
                    let props = BasicProperties::default();
                    let popts = BasicPublishOptions::default();
                    let ch = ch_opt.unwrap();
                    let payload = format!("Hello World {}!", i).as_bytes().to_vec();
                    assert!(ch.status().connected());
                    let confirm = ch.basic_publish(
                        "my_exchange",
                        "test_route",
                        popts,
                        payload,
                        props
                    ).await;
                    assert!(confirm.is_ok());
                    {
                        let mut charc = rpoold.lock().unwrap();
                        charc.release(ch);
                    }
                }else{
                    assert!(false);
                }
            });
        }
    }

    #[test]
    fn should_create_and_reuse_connections_with_spawns(){
        let runtime = get_runtime();
        let new_arc = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc).await;
            let pool = get_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpool = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpool.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = ExchangeDeclareOptions::default();
                excopts.nowait = false;
                excopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                let confirm = ch.exchange_declare(
                    "my_exchange",
                    ExchangeKind::Direct,
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpool.lock().unwrap();
                    charc.release(ch);
                }
            }else{
                assert!(false);
            }
        });
        let new_arc2 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc2).await;
            let pool = get_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolb = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolb.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut qopts = QueueDeclareOptions::default();
                qopts.nowait = false;
                qopts.durable = true;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.queue_declare(
                    "my_queue",
                    qopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolb.lock().unwrap();
                    charc.release(ch);
                }
            }else{
                assert!(false);
            }
        });
        let new_arc3 = runtime.clone();
        runtime.block_on(async move{
            let mut conn_pool = get_connection_pool(new_arc3).await;
            let pool = get_channel_pool(&mut conn_pool).await;
            let ch_pool = Arc::new(Mutex::new(pool));
            let rpoolc = ch_pool.clone();
            let ch_opt = {
                let mut charc = rpoolc.lock().unwrap();
                charc.get_channel()
            };
            if ch_opt.is_some(){
                let mut excopts = QueueBindOptions::default();
                excopts.nowait = false;
                let ftable = FieldTable::default();
                let ch = ch_opt.unwrap();
                assert!(ch.status().connected());
                let confirm = ch.queue_bind(
                    "my_queue",
                    "my_exchange",
                    "test_route",
                    excopts,
                    ftable
                ).await;
                assert!(confirm.is_ok());
                {
                    let mut charc = rpoolc.lock().unwrap();
                    charc.release(ch);
                }
            }
        });
        let sema = Arc::new(Semaphore::new(2));
        let mut cvec = Vec::<JoinHandle<()>>::new();
        for i in 0..20000 {
            let new_arc4 = runtime.clone();
            let rsema = sema.clone();
            let h = runtime.spawn(async move {
                let mut conn_pool = get_connection_pool(new_arc4).await;
                let pool = get_single_channel_pool(&mut conn_pool).await;
                let ch_pool = Arc::new(Mutex::new(pool));
                let rpoold = ch_pool.clone();
                let permit = rsema.acquire().await.unwrap();
                permit.forget();
                let ch_opt = {
                    let mut charc = rpoold.lock().unwrap();
                    charc.get_channel()
                };
                if ch_opt.is_some() {
                    let props = BasicProperties::default();
                    let popts = BasicPublishOptions::default();
                    let ch = ch_opt.unwrap();
                    let payload = format!("Hello World {}!", i).as_bytes().to_vec();
                    assert!(ch.status().connected());
                    let confirm = ch.basic_publish(
                        "my_exchange",
                        "test_route",
                        popts,
                        payload,
                        props
                    ).await;
                    assert!(confirm.is_ok());
                    {
                        let mut charc = rpoold.lock().unwrap();
                        charc.release(ch);
                    }
                }else{
                    assert!(false);
                }
                rsema.add_permits(1);
            });
            cvec.push(h);
        }
        runtime.block_on(async move {
            for handle in cvec{
                let _r = handle.await;
            }
        });
    }
}