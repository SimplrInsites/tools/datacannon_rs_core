/// RabbitMQ connection utilities
///
/// ---
/// author: Andrew Evans
/// ---

use std::time::Duration;

use lapin::{
    Connection, ConnectionProperties};
use tokio;
use tokio::time::timeout;
use tokio_amqp::*;

use crate::connection::amqp::connection_inf::AMQPConnectionInf;
use crate::error::connection_failed::ConnectionFailed;

/// Get a standard tcp `lapin::Connection` without SSL asynchronously
///
/// # Arguments
/// * `addr` - URI for the connection
async fn get_standard_connection(addr: &str) -> Result<Connection, ConnectionFailed>{
    let conn_pinky = Connection::connect(
        addr.into(), ConnectionProperties::default().with_tokio());
    let conn_result = conn_pinky.await;
    if conn_result.is_ok() {
        let conn = conn_result.ok().unwrap();
        Ok(conn)
    }else{
        Err(ConnectionFailed)
    }
}


/// Get a connection asynchronously
///
/// # Arguments
/// * `conn_inf` - The connection information
pub async fn get_connection(conn_inf: &AMQPConnectionInf) -> Result<Connection, ConnectionFailed>{
    let uri = conn_inf.to_url();
    let conn_result = timeout(Duration::from_millis(
        conn_inf.get_conection_timeout()),get_standard_connection( uri.as_str())).await;
    if conn_result.is_ok() {
        let conn = conn_result.unwrap();
        if conn.is_ok() {
            Ok(conn.unwrap())
        }else{
            Err(ConnectionFailed)
        }
    } else {
        eprintln!("Connection Failed: {}", conn_result.err().unwrap());
        Err(ConnectionFailed)
    }
}


#[cfg(test)]
pub mod test{
    use std::env;

    use super::*;
    use std::sync::Arc;

    #[test]
    fn test_should_connect(){
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let rtr = tokio::runtime::Builder::new_multi_thread().enable_all().worker_threads(4).build();
        let  rt = rtr.unwrap();
        let arc_rt = Arc::new(rt);
        arc_rt.clone().block_on(async move{
            let uri = amq_conf.to_url();
            let conn =  Connection::connect(
                &uri, ConnectionProperties::default().with_tokio()).await;
            let channel = conn.ok().unwrap().create_channel().await;
            channel.ok().unwrap();
        });
    }
}
