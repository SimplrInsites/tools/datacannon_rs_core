//! Consumer creation error
//!
//! ---
//! author: Andrew Evans
//! ---

use std::fmt;

/// Thrown when the broker failed to create
pub struct ConsumerCreationError;

/// Display implementation for when the broker failed to start
impl fmt::Display for ConsumerCreationError{

    /// Display the standard error message
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to Create Consumer")
    }
}


/// Debut for PoolIsEmptyError
impl fmt::Debug for ConsumerCreationError{

    /// Display the debug information for the programmer
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
    }
}
