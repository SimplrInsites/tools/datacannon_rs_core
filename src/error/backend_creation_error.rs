//!Exchange related error
//!
//! ---
//! author: Andrew Evans
//! ---

use std::fmt;

/// Thrown when the connection pool is empty
pub struct BackendCreationError;

/// Display implementation for when the pool is empty
impl fmt::Display for BackendCreationError{

    /// Display the standard error message
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Wrong Backend Type")
    }
}


/// Debut for PoolIsEmptyError
impl fmt::Debug for BackendCreationError{

    /// Display the debug information for the programmer
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
    }
}
