//! Broker creation error
//!
//! ---
//! author: Andrew Evans
//! ---

use std::fmt;

/// Thrown when the broker failed to create
pub struct BrokerCreationError;

/// Display implementation for when the broker failed to start
impl fmt::Display for BrokerCreationError{

    /// Display the standard error message
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Failed to Create Broker")
    }
}


/// Debut for PoolIsEmptyError
impl fmt::Debug for BrokerCreationError{

    /// Display the debug information for the programmer
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
    }
}
