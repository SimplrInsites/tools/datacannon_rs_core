//! Error for broker started
//!
//! ---
//! author: Andrew Evans
//! ---

use std::fmt;

/// Thrown when the broker fails to start
pub struct BrokerStartError;

/// Display implementation for when broker fails to start
impl fmt::Display for BrokerStartError{

    /// Display the standard error message
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Broker Failed to Start")
    }
}


/// Debut for PoolIsEmptyError
impl fmt::Debug for BrokerStartError{

    /// Display the debug information for the programmer
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
    }
}
