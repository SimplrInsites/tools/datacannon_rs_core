//! Arg types for Rust. Allows generic argument storage.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{BTreeMap, HashMap};

use amq_protocol_types::*;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

use crate::AmqpValue;

///Argument Type enum used to store generic values
#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum ArgType{
    ArgVec(Vec<ArgType>),
    ArgMap(HashMap<String, ArgType>),
    Bool(bool),
    String(String),
    Char(char),
    Int(i64),
    Long(f64),
    Double(f32),
    Usize(usize),
    Bytes(Vec<u8>),
    StringArray(Vec<String>),
    IntArray(Vec<i64>),
    LongArray(Vec<f64>),
    DoubleArray(Vec<f32>),
    CharArray(Vec<char>),
    SizeArray(Vec<usize>),
    Map(HashMap<String, ArgType>),
    U64(u64),
    NULL,
}


/// Convert an amqp value to an argtype
///
/// # Arguments
/// * `amqp_value` - The amqp value
pub fn amqp_value_to_arg_type(amqp_value: AMQPValue) -> Result<ArgType, ()> {
    if let AMQPValue::FieldTable(amqp_value) = amqp_value{
        let map = amqp_value.inner();
        let hmap = amqp_map_to_arg_map(map);
        if hmap.is_ok(){
            Ok(ArgType::ArgMap(hmap.ok().unwrap()))
        }else{
            Err(())
        }
    }else if let AMQPValue::FieldArray(amqp_value) = amqp_value{
        let arg_vec = amqp_array_to_arg_vec(amqp_value.as_slice());
        if arg_vec.is_ok(){
            Ok(ArgType::ArgVec(arg_vec.ok().unwrap()))
        }else{
            Err(())
        }
    }else if let AMQPValue::ShortString(amqp_value) = amqp_value{
        Ok(ArgType::String(amqp_value.as_str().to_string()))
    }else if let AMQPValue::Boolean(amqp_value) = amqp_value{
        Ok(ArgType::Bool(amqp_value))
    }else if let AMQPValue::ByteArray(amqp_value) = amqp_value{
        Ok(ArgType::Bytes(Vec::from(amqp_value.as_slice().clone())))
    }else if let AMQPValue::DecimalValue(amqp_value) = amqp_value{
        let mut v = amqp_value.value as f32;
        let s = amqp_value.scale as f32;
        v = v / (10.0 as f32).powf(s);
        Ok(ArgType::Double(v))
    }else if let AMQPValue::Float(amqp_value) = amqp_value{
        Ok(ArgType::Double(amqp_value))
    }else if let AmqpValue::LongInt(amqp_value) = amqp_value{
        Ok(ArgType::Int(amqp_value as i64))
    }else if let AmqpValue::LongLongInt(amqp_value) = amqp_value{
        Ok(ArgType::Int(amqp_value as i64))
    }else if let AmqpValue::LongString(amqp_value) = amqp_value{
        let vstr = amqp_value.as_str().to_string();
        Ok(ArgType::String(vstr))
    }else if let AMQPValue::ShortInt(amqp_value) = amqp_value{
        Ok(ArgType::Int(amqp_value as i64))
    }else if let AMQPValue::ShortShortInt(amqp_value) = amqp_value{
        Ok(ArgType::Int(amqp_value as i64))
    } else if let AMQPValue::LongUInt(amqp_value) = amqp_value{
        Ok(ArgType::Usize(amqp_value as usize))
    }else if let AMQPValue::ShortShortUInt(amqp_value) = amqp_value{
        Ok(ArgType::Usize(amqp_value as usize))
    }else if let AMQPValue::ShortUInt(amqp_value) = amqp_value{
        Ok(ArgType::Usize(amqp_value as usize))
    }else if let AMQPValue::Void = amqp_value{
        Ok(ArgType::NULL)
    }else if let AMQPValue::Timestamp(amqp_value) = amqp_value{
        Ok(ArgType::U64(amqp_value))
    }else {
        Err(())
    }
}


/// Convert an amqp array to an arg vec
///
/// # Arguments
/// * `amqp_values` - Array of amqp values
pub fn amqp_array_to_arg_vec(amqp_values: &[AMQPValue]) -> Result<Vec<ArgType>, ()>{
    let mut arg_vec = vec![];
    let mut is_fail = false;
    for av in amqp_values{
        let arg = amqp_value_to_arg_type(av.clone());
        if arg.is_ok(){
            arg_vec.push(arg.ok().unwrap());
        }else{
            is_fail = true;
            break;
        }
    }
    if is_fail{
        Err(())
    }else{
        Ok(arg_vec)
    }
}


/// Convert an amqp map to an arg map.
///
/// # Arguments
/// * `amqp_map` - Mapping of amqp values
pub fn amqp_map_to_arg_map(amqp_map: &BTreeMap<ShortString, AMQPValue>) -> Result<HashMap<String, ArgType>, ()>{
    let mut arg_map = HashMap::<String, ArgType>::new();
    let mut is_fail = false;
    for (k, v) in amqp_map{
        let key_str = k.as_str().to_string();
        let val = amqp_value_to_arg_type(v.clone());
        if val.is_ok(){
            arg_map.insert(key_str, val.ok().unwrap());
        }else{
            is_fail = true;
            break;
        }
    }
    if is_fail{
        Err(())
    }else{
        Ok(arg_map)
    }
}


/// Convert a serde vale to an ArgType. Returns an error if anything failed to convert. Does not
/// convert vectors of values. This is the domain of `value_vec_to_arg_vec`.
///
/// # Arguments
/// * `val` - Serde value to convert
pub fn value_to_arg(val: Value) -> Result<ArgType, ()>{
    if val.is_null(){
        Ok(ArgType::NULL)
    }else if let Value::Bool(val) = val{
        Ok(ArgType::Bool(val))
    }else if let Value::Number(val) = val{
        if val.is_f64(){
            let arg = ArgType::Long(val.as_f64().unwrap());
            Ok(arg)
        }else if val.is_u64(){
            let arg = ArgType::U64(val.as_u64().unwrap());
            Ok(arg)
        }else if val.is_i64(){
            let arg = ArgType::Int(val.as_i64().unwrap());
            Ok(arg)
        }else{
            Err(())
        }
    }else if let Value::Object(val) = val {
        let map_res = value_to_arg_map(val);
        if map_res.is_ok(){
            let arg = ArgType::ArgMap(map_res.ok().unwrap());
            Ok(arg)
        }else{
            Err(())
        }
    }else if let Value::String(val) = val{
        let arg = ArgType::String(val);
        Ok(arg)
    }else if let Value::Array(val) = val{
        let arg_result = value_vec_to_arg_vec(val);
        if arg_result.is_ok(){
            let arg = ArgType::ArgVec(arg_result.ok().unwrap());
            Ok(arg)
        }else {
            Err(())
        }
    }else{
        Err(())
    }

}


/// Convert a value to an argument map
///
/// # Arguments
/// `v` - Value map to create
pub fn value_to_arg_map(vmap: Map<String, Value>) -> Result<HashMap<String, ArgType>, ()>{
    let mut arg_map = HashMap::<String, ArgType>::new();
    let mut is_fail = false;
    for (k,v) in vmap{
        let arg_result = value_to_arg(v);
        if arg_result.is_ok(){
            let arg = arg_result.ok().unwrap();
            arg_map.insert(k, arg);
        }else{
            is_fail = true;
            break;
        }
    }
    if is_fail {
        Err(())
    }else{
        Ok(arg_map)
    }
}


/// Convert a vec of values to an arg vec.
///
/// # Arguments
/// * `val_vec` - Vector of serde json values
pub fn value_vec_to_arg_vec(val_vec: Vec<Value>) -> Result<Vec<ArgType>, ()>{
    let mut arg_vec = Vec::<ArgType>::new();
    let mut is_fail = false;
    for v in val_vec{
        if v.is_array(){
            let vvec = v.as_array().unwrap();
            let av = value_vec_to_arg_vec(vvec.clone());
            if av.is_ok(){
                let avec = av.ok().unwrap();
                arg_vec.push(ArgType::ArgVec(avec));
            }else{
                is_fail = true;
            }
        }else {
            let arg = value_to_arg(v);
            if arg.is_ok() {
                arg_vec.push(arg.ok().unwrap());
            } else {
                is_fail = true;
                break;
            }
        }
    }
    if is_fail{
        Err(())
    }else{
        Ok(arg_vec)
    }
}


/// Convert an argument to an `AMQPValue`
pub fn arg_to_amqp_value(arg: ArgType) -> AmqpValue{
    match arg{
        ArgType::Map(arg) => {
            let mut val_map = BTreeMap::<ShortString, AmqpValue>::new();
            for (key, val) in &arg{
                let amqp_val = arg_to_amqp_value(val.clone());
                let short_key = ShortString::from(key.clone());
                val_map.insert(short_key, amqp_val);
            }
            AMQPValue::FieldTable(amq_protocol_types::FieldTable::from(val_map))
        },
        ArgType::Bool(arg) =>{
            if arg {
                AmqpValue::Boolean(true)
            }else{
                AmqpValue::Boolean(false)
            }
        },
        ArgType::String(arg) =>{
            AmqpValue::LongString(LongString::from(arg))
        },
        ArgType::Char(arg) => {
            let cstr = format!("{}", arg);
            AmqpValue::LongString(LongString::from(cstr))
        },
        ArgType::Int(arg) =>{
            AmqpValue::LongLongInt(amq_protocol_types::LongLongInt::from(arg))
        },
        ArgType::Long(arg) => {
            AmqpValue::Double(amq_protocol_types::Double::from(arg))
        },
        ArgType::Double(arg) =>{
            AmqpValue::Float(amq_protocol_types::Float::from(arg))
        },
        ArgType::Bytes(arg) =>{
            AmqpValue::ByteArray(amq_protocol_types::ByteArray::from(arg))
        },
        ArgType::StringArray(arg) => {
            let mut amq_vec = Vec::<AmqpValue>::new();
            for i in 0..arg.len() {
                let arg_str = arg.get(i).unwrap().clone();
                amq_vec.push(AmqpValue::LongString(amq_protocol_types::LongString::from(arg_str)));
            }
            AmqpValue::FieldArray(amq_protocol_types::FieldArray::from(amq_vec))
        },
        ArgType::IntArray(arg) => {
            let mut amq_vec = Vec::<AmqpValue>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(AmqpValue::LongLongInt(n));
            }
            AmqpValue::FieldArray(amq_protocol_types::FieldArray::from(amq_vec))
        },
        ArgType::LongArray(arg) => {
            let mut amq_vec = Vec::<AmqpValue>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(AmqpValue::Double(n));
            }
            AmqpValue::FieldArray(amq_protocol_types::FieldArray::from(amq_vec))
        },
        ArgType::DoubleArray(arg) =>{
            let mut amq_vec = Vec::<AmqpValue>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(AmqpValue::Float(n));
            }
            AmqpValue::FieldArray(amq_protocol_types::FieldArray::from(amq_vec))
        },
        ArgType::CharArray(arg) => {
            let mut amq_vec = Vec::<AmqpValue>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                let cstr = format!("{}", n);
                amq_vec.push(AmqpValue::LongString(amq_protocol_types::LongString::from(cstr)));
            }
            AmqpValue::FieldArray(amq_protocol_types::FieldArray::from(amq_vec))
        },
        ArgType::U64(arg) =>{
            AmqpValue::Timestamp(amq_protocol_types::Timestamp::from(arg))
        },
        _ => {
            AmqpValue::Void
        },
    }
}


/// Convert an argument to a serde `Value`
pub fn arg_to_value(arg: ArgType) -> Value{
    match arg{
        ArgType::Map(arg) => {
            let mut val_map = Map::new();
            for (key, val) in &arg{
                let serde_val = arg_to_value(val.clone());
                val_map.insert(String::from(key), serde_val);
            }
            Value::Object(val_map)
        },
        ArgType::NULL => {
            Value::Null
        },
        ArgType::U64(arg)  => {
          Value::from(arg)
        },
        ArgType::Bool(arg) =>{
            Value::from(arg)
        },
        ArgType::String(arg) =>{
            Value::from(arg)
        },
        ArgType::Char(arg) => {
            let str = arg.to_string();
            Value::from(str)
        },
        ArgType::Int(arg) =>{
            Value::from(arg)
        },
        ArgType::Long(arg) => {
            Value::from(arg)
        },
        ArgType::Double(arg) =>{
            Value::from(arg)
        },
        ArgType::Bytes(arg) =>{
            let mut amqp_vec = Vec::<Value>::new();
            for i in 0..arg.len(){
                let ssui = arg.get(i).unwrap().clone();
                amqp_vec.push(Value::from(ssui));
            }
            Value::Array(amqp_vec)
        },
        ArgType::StringArray(arg) => {
            let mut amq_vec = Vec::<Value>::new();
            for i in 0..arg.len() {
                let str = arg.get(i).unwrap().clone();
                amq_vec.push(Value::from(str));
            }
            Value::from(amq_vec)
        },
        ArgType::IntArray(arg) => {
            let mut amq_vec = Vec::<Value>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(Value::from(n));
            }
            Value::from(amq_vec)
        },
        ArgType::LongArray(arg) => {
            let mut amq_vec = Vec::<Value>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(Value::from(n));
            }
            Value::from(amq_vec)
        },
        ArgType::DoubleArray(arg) =>{
            let mut amq_vec = Vec::<Value>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                amq_vec.push(Value::from(n));
            }
            Value::from(amq_vec)
        },
        ArgType::CharArray(arg) => {
            let mut amq_vec = Vec::<Value>::new();
            for i in 0..arg.len() {
                let n = arg.get(i).unwrap().clone();
                let cstr = format!("{}", n);
                amq_vec.push(Value::from(cstr));
            }
            Value::from(amq_vec)
        },
        _ =>{
            Value::Null
        }
    }
}


#[cfg(test)]
pub mod test{
    use serde_json;
    use serde_json::Value;

    use crate::argparse::argtype::{value_to_arg, value_vec_to_arg_vec};

    #[test]
    fn should_convert_serde_value_to_argtype(){
        let tarr = r#"[[1, "hi"],{"tint": 1},{"callbacks":[],"chain":[],"chord":[],"errbacks":[]}]"#;
        let barr = tarr.as_bytes();
        let jobj: Vec<Value> = serde_json::from_slice(barr).ok().unwrap();
        for v in jobj {
            println!("{}", serde_json::to_string(&v.clone()).ok().unwrap());
            let vres = value_to_arg(v);
            assert!(vres.is_ok());
        }
    }

    #[test]
    fn should_convert_value_vec_to_arg_vec(){
        let tarr = r#"[[1, "hi"],{"tint": 1},{"callbacks":[],"chain":[],"chord":[],"errbacks":[]}]"#;
        let barr = tarr.as_bytes();
        let jobj: Vec<Value> = serde_json::from_slice(barr).ok().unwrap();
        let jres = value_vec_to_arg_vec(jobj);
        assert!(jres.is_ok());
    }
}